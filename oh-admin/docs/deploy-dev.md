# Deployment - DEV

```bash
sudo docker-compose up -d --build mongo
```

```bash
mkdir data/nginx-conf
mkdir data/certbot
mkdir data/certbot/cert
mkdir data/certbot/cert/live
mkdir data/certbot/cert/live/cad.hishk.com
mkdir data/certbot/cert/live/cadapi.hishk.com

sudo docker-compose run --rm --entrypoint "\
  openssl req -x509 -nodes -newkey rsa:4096 -days 1\
    -keyout '/etc/letsencrypt/live/cad.hishk.com/privkey.pem' \
    -out '/etc/letsencrypt/live/cad.hishk.com/fullchain.pem' \
    -subj '/CN=localhost'" certbot

sudo docker-compose run --rm --entrypoint "\
  openssl req -x509 -nodes -newkey rsa:4096 -days 1\
    -keyout '/etc/letsencrypt/live/cadapi.hishk.com/privkey.pem' \
    -out '/etc/letsencrypt/live/cadapi.hishk.com/fullchain.pem' \
    -subj '/CN=localhost'" certbot

sudo docker-compose run --rm --entrypoint "\
  rm -Rf /etc/letsencrypt/live/cad.hishk.com && \
  rm -Rf /etc/letsencrypt/archive/cad.hishk.com && \
  rm -Rf /etc/letsencrypt/renewal/cad.hishk.com.conf" certbot

sudo docker-compose run --rm --entrypoint "\
  rm -Rf /etc/letsencrypt/live/cadapi.hishk.com && \
  rm -Rf /etc/letsencrypt/archive/cadapi.hishk.com && \
  rm -Rf /etc/letsencrypt/renewal/cadapi.hishk.com.conf" certbot


sudo docker-compose run --rm --entrypoint "\
  certbot certonly --webroot -w /var/www/certbot \
    --email jason.ching@hishk.com \
    -d cad.hishk.com \
    --rsa-key-size 4096 \
    --agree-tos \
    --force-renewal" certbot

sudo docker-compose run --rm --entrypoint "\
  certbot certonly --webroot -w /var/www/certbot \
    --email jason.ching@hishk.com \
    -d cadapi.hishk.com \
    --rsa-key-size 4096 \
    --agree-tos \
    --force-renewal" certbot


sudo docker-compose run --rm --entrypoint "\
  certbot renew" certbot
```

## Server Info

IP: 18.136.238.243

## Docker Containers

```mermaid
flowchart TB
  L(Let's Encrypt)
  B(Browser)
  B -- https://vqadmin-dev.hishk.com:443<br />https://vqnode-dev.hishk.com:443 --> N
  L -- Challenge Port 80 --> N
  CB -..-> L
  subgraph Docker
    N(Nginx)
    DB(MongoDB)
    A(NextJS Admin)
    Node(NodeJS API)
    CB(CertBot)
    CF{{Cert File}}

    N -- next-admin:3000 --> A
    N -- node-1:5001 --> Node
    CB --> CF
    CF --> N
    Node --> DB
  end
```

### Containers

|         | Name          |
| ------- | ------------- |
| MongoDB | mongo         |
| Node    | node-1 (5001) |
| Admin   | next-admin    |
| Certbot | certbot       |
| Nginx   | nginx         |

## Build

### Node

```bash
cd her-node
yarn build
```

Output Folder: `her-node/build-dist/build`

Copy the build folder to `docker/server` in `hernode-dev`, docker

Then rebuild:

```bash
sudo docker compose up -d --build node-1 node-2
```

```bash
sudo docker ps
sudo docker logs node-1
sudo docker logs node-2
```

### Admin

Copy the whole source code over to `build-admin`

### Admin Only

```bash
sudo docker system prune -f
sudo docker stop next-admin
sudo docker compose up -d --build next-admin
sudo docker start next-admin
sudo docker restart nginx
sudo docker system prune -f
```
