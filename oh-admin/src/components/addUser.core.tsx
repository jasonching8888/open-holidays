import { useForm } from "react-hook-form";

import { useTranslation } from "../core/translation.core";
import userCore from "../core/user.core";
// import { AdminUserRole, AdminUserStatus } from "../types/user";
// import { AdminUser } from "../types/user";

interface AddUserForm {
  id: string;
  email: string;
  name: string;
  userRole: AdminUserRole;
  status: AdminUserStatus;
}

export default function AddUser() {
  const { t, msg } = useTranslation();
  const { register, handleSubmit } = useForm<AddUserForm>();

  async function onSubmit(data: AddUserForm) {
    await userCore.addUser(convertToAdminUser(data));
  }

  return (
    <div className="my-6">
      <form id="add-user" onSubmit={handleSubmit(onSubmit)}>
        <div className="flex">
          <input type="text" className="h-9 border" placeholder={t(msg.setting.email)} {...register("email")} />
          {/* <select {...register("type")}>
            {Object.values(ShopUserType).map((t) => (
              <option key={t} value={t}>
                {userCore.getShopUserTypeText(t)}
              </option>
            ))}
          </select> */}
        </div>
        <input type="submit" value={t(msg.user.addUser)} />
      </form>
    </div>
  );
}
function convertToAdminUser(data: AddUserForm): AdminUserFormData {
  return null;
}
