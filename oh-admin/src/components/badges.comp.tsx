import { UseFormReturn, useWatch } from "react-hook-form";

import CloseIcon from "./iconClose.comp";

export default function Badges({
  methods,
  fieldName,
  options,
}: {
  methods: UseFormReturn<any, any>;
  fieldName: string;
  options: { name: string; value: string }[];
}) {
  const values = useWatch({ control: methods.control, name: fieldName });

  if (!values) return null;

  return (
    <>
      {values.map((v) => (
        <Badge key={v} fieldName={fieldName} value={v} options={options} methods={methods} />
      ))}
    </>
  );
}

const Badge = ({
  value,
  fieldName,
  methods,
  options,
}: {
  value: string;
  fieldName: string;
  methods: UseFormReturn<any, any>;
  options: { name: string; value: string }[];
}) => {
  const name = options.find((o) => o.value === value).name;

  function deleteValue() {
    const values = methods.getValues(fieldName) as string[];

    if (values === undefined) return;

    const newValues = values.filter((v) => v !== value);
    methods.setValue(fieldName, newValues);
  }

  return (
    <div className="flex items-center mt-2 px-5 py-2.5 text-sm font-medium text-center text-white bg-blue-700 rounded-lg focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:focus:ring-blue-800">
      {name}
      <button
        className="flex items-center justify-center w-4 h-4 ml-2 text-xs font-semibold text-blue-800 bg-blue-200 rounded-full hover:bg-red-400 hover:text-white"
        onClick={deleteValue}
      >
        <CloseIcon />
      </button>
    </div>
  );
};
