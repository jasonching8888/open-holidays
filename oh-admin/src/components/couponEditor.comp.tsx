import { UseFormReturn } from "react-hook-form";
import { EditingMode, FancySelect, LabelAndInput } from "./form.comp";
import WarningAlert from "./warningAlert.comp";
import SuccessAlert from "./successAlert.comp";
import { useQuery } from "@tanstack/react-query";
import { getRestaurantList } from "../core/restaurant.core";
import { useTranslation } from "../core/translation.core";

export default function CouponEditor({
  editingMode,
  methods,
  onSubmit,
  jobMutation,
}: {
  editingMode: EditingMode;
  methods: UseFormReturn<CouponForm, any>;
  onSubmit: (data: CouponForm) => void;
  jobMutation: any;
}) {
  return (
    <div className="flex justify-center ">
      <div className="min-w-full flex flex-col items-center ">
        <h1>Add / Update Coupon</h1>
        {jobMutation?.isError && <WarningAlert message="Error" />}
        {jobMutation?.isSuccess && (
          <SuccessAlert message={`Job ${editingMode == EditingMode.create ? "added" : "edited"} successfully`} />
        )}

        <form className="flex flex-col items-center min-w-full" onSubmit={methods.handleSubmit(onSubmit)}>
          <div className="md:w-1/2 w-full">
            <LabelAndInput register={methods.register} fieldName="code" label="Code" />
            <LabelAndInput register={methods.register} fieldName="discount" type="number" label="Discount" />
            <RestaurantSelector methods={methods} />
            <div>
              <label htmlFor="status">Status</label>
              <FancySelect
                register={methods.register}
                fieldName="status"
                options={[
                  { value: "enabled", label: "Enabled" },
                  { value: "disabled", label: "Disabled" },
                ]}
              />
            </div>
            <LimitSection methods={methods} />
          </div>

          <SubmitButton editingMode={editingMode} />
        </form>
      </div>
    </div>
  );
}

function SubmitButton({ editingMode }: { editingMode: EditingMode }) {
  const { t, msg } = useTranslation();

  function getSubmitBtnText() {
    switch (editingMode) {
      case EditingMode.create:
        return t(msg.client.add);
      case EditingMode.edit:
        return t(msg.client.edit);
    }
  }

  return (
    <div>
      <input className=" bg-blue-500 text-white m-2 " type="submit" value={getSubmitBtnText()} />
    </div>
  );
}

const RestaurantSelector = ({ methods }: { methods: UseFormReturn<CouponForm, any> }) => {
  const { data, isFetched } = useQuery({
    queryKey: ["restaurants"],
    queryFn: () => getRestaurantList(),
  });

  if (!isFetched) return null;

  return (
    <div>
      <label htmlFor="restaurantId">Restaurant</label>
      <FancySelect
        register={methods.register}
        fieldName="restaurantId"
        options={data.map((restaurant) => {
          return {
            label: restaurant.name,
            value: restaurant.id,
          };
        })}
      />
    </div>
  );
};

const LimitSection = ({ methods }: { methods: UseFormReturn<CouponForm, any> }) => {
  return (
    <div className="mt-4">
      <h1>Limit Setting</h1>
      <LabelAndInput register={methods.register} fieldName="usageLimit" label="Usage Limit" />
    </div>
  );
};
