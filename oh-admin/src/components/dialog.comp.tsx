import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useState } from "react";

import IconCloseModal from "../components/iconCloseModal.comp";
import IconWarning from "../components/iconWarning";
import { deleteAction } from "../core/fetch.core";

interface MutationParams {
  url: string;
  id: string;
}

export default function Dialog({ id, title, url, queryKey }) {
  const [clicked, setClicked] = useState(false);
  const queryClient = useQueryClient();

  const deleteMutation = useMutation({
    mutationFn: async (data: MutationParams) => await deleteAction(data.url, data.id),
    onSuccess: (data) => {
      if (data.status === 200) {
        queryClient.invalidateQueries({ queryKey:[queryKey] });
      }
    },
  });

  const handleClick = (e) => {
    e.preventDefault();
    setClicked(true);
  };

  const close = (e) => {
    e.preventDefault();

    setClicked(false);
  };

  const handleDelete = async (e) => {
    e.preventDefault();
    const data = await deleteMutation.mutate({ url, id });
    console.log(data);
    setClicked(false);
  };
  return (
    <>
      <button
        onClick={handleClick}
        className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"
        type="button"
      >
        Delete
      </button>

      <div
        id={id}
        tabIndex={-1}
        className={`${clicked ? "block" : "hidden"} fixed top-5 inset-0 flex justify-center z-50`}
      >
        <div className="relative w-full h-full max-w-2xl md:h-auto">
          <div className="relative bg-white rounded-lg shadow dark:bg-gray-700">
            <button
              type="button"
              className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
              onClick={close}
            >
              <IconCloseModal />
              <span className="sr-only">Close modal</span>
            </button>
            <div className="p-6 text-center">
              <IconWarning />
              <h3 className="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400">
                Are you sure you want to delete this {title}?
              </h3>
              <button
                onClick={handleDelete}
                data-modal-hide="popup-modal"
                type="button"
                className="text-white bg-red-600 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 dark:focus:ring-red-800 font-medium rounded-lg text-sm inline-flex items-center px-5 py-2.5 text-center mr-2"
              >
                Yes, I{`'`}m sure
              </button>
              <button
                onClick={close}
                type="button"
                className="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-gray-200 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600"
              >
                No, cancel
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
