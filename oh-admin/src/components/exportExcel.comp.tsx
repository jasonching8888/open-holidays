import XLSX from "xlsx/dist/xlsx.full.min.js";
export default function ExcelExport({
  name,
  excelSheets,
  columnsToExclude,
}: {
  name: string;
  excelSheets: string[];
  columnsToExclude?: number[];
}) {
  const exportToExcel = () => {
    const tables = document.querySelectorAll("table");
    const isTablesEmpty = tables.length === 0;

    if (isTablesEmpty) {
      return;
    }

    const workbook = XLSX.utils.book_new();

    tables.forEach((table, index) => {
      const worksheet = XLSX.utils.table_to_sheet(table, { raw: true });

      const columnLengths = [];
      XLSX.utils.sheet_to_json(worksheet, { header: 1 }).forEach((row) => {
        row.forEach((cell, colIndex) => {
          const cellLength = cell ? cell.toString().length : 10;
          if (!columnLengths[colIndex] || cellLength > columnLengths[colIndex]) {
            columnLengths[colIndex] = cellLength;
          }
        });
      });

      worksheet["!cols"] = columnLengths.map((length) => ({ width: length + 2 }));

      if (columnsToExclude?.length > 0) {
        columnsToExclude.forEach((col) => {
          worksheet["!cols"][col] = { hidden: true };
        });
      }

      XLSX.utils.book_append_sheet(workbook, worksheet, excelSheets[index]);
    });

    const excelBuffer = XLSX.write(workbook, { bookType: "xlsx", type: "array" });
    const data = new Blob([excelBuffer], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    });
    const url = URL.createObjectURL(data);
    const link = document.createElement("a");
    link.href = url;
    link.download = `${name}.xlsx`;
    link.click();
    URL.revokeObjectURL(url);
  };

  return <ExportButton name={name} exportToExcel={exportToExcel} />;
}

export function ExcelExportArray<T>({
  name,
  getArray,
  filterValues,
}: {
  name: string;
  getArray: (data: T) => Promise<Object[]>;
  filterValues?: Object;
}) {
  const exportToExcel = async () => {
    const dataArray = await getArray(filterValues as T);

    if (dataArray.length === 0) {
      return;
    }

    const workbook = XLSX.utils.book_new();
    const worksheet = XLSX.utils.json_to_sheet(dataArray);

    const headerRange = XLSX.utils.decode_range(worksheet["!ref"]);
    for (let col = headerRange.s.c; col <= headerRange.e.c; col++) {
      const headerCell = XLSX.utils.encode_cell({ r: headerRange.s.r, c: col });
      const headerCellValue = worksheet[headerCell]?.v;
      if (typeof headerCellValue === "string") {
        worksheet[headerCell].v = capitalizeFirstLetter(headerCellValue);
      }
    }

    const columnLengths = [];
    XLSX.utils.sheet_to_json(worksheet, { header: 1 }).forEach((row) => {
      row.forEach((cell, colIndex) => {
        const cellLength = cell ? cell.toString().length : 10;
        if (!columnLengths[colIndex] || cellLength > columnLengths[colIndex]) {
          columnLengths[colIndex] = cellLength;
        }
      });
    });

    worksheet["!cols"] = columnLengths.map((length) => ({ width: length + 2 }));

    XLSX.utils.book_append_sheet(workbook, worksheet, name);

    const excelBuffer = XLSX.write(workbook, { bookType: "xlsx", type: "array" });
    const data = new Blob([excelBuffer], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    });
    const url = URL.createObjectURL(data);
    const link = document.createElement("a");
    link.href = url;
    link.download = `${name.replace(/ /g, "_")}.xlsx`;
    link.click();
    URL.revokeObjectURL(url);
  };

  return <ExportButton name={name} exportToExcel={exportToExcel} />;
}

const ExportButton = ({ name, exportToExcel }) => {
  return (
    <button
      type="button"
      className={`capitalize text-white ml-4 bg-gradient-to-r from-blue-500 via-blue-600 to-blue-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2`}
      onClick={exportToExcel}
    >
      Download {name}
    </button>
  );
};

function capitalizeFirstLetter(input: string): string {
  const words = input.split(/\s+/);
  const capitalizedWords = words.map((word) => word.charAt(0).toUpperCase() + word.slice(1));
  return capitalizedWords.join(" ");
}
