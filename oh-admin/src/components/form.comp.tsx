import { Path, RegisterOptions, UseFormRegister } from "react-hook-form";

export enum EditingMode {
  create,
  edit,
}

export function LabelAndInput<T>({
  register,
  label,
  fieldName,
  type = "text",
}: {
  register: UseFormRegister<T>;
  label: string;
  fieldName: Path<T>;
  type?: string;
}) {
  return (
    <label className="block mt-4">
      {label}
      <FancyTextInput type={type} register={() => register(fieldName as any)} placeholder={label} />
    </label>
  );
}

export function FancyCheckbox({
  labelId,
  value,
  register,
  children,
}: {
  labelId?: string;
  value: string;
  register: any;
  children: React.ReactNode;
}) {
  const inputCss =
    "mr-2 " +
    "appearance-none " + // hide the original checkbox
    "before:h-4 before:w-4 before:border before:hover:border-2 before:border-gray-400 before:block before:opactiy-100 before:rounded " + // style when not checked
    "before:hover:border-blue-600 " + // change broder when hover.  make sure no transition duration is applied
    "before:checked:bg-blue-600 before:checked:border-0 " + // style when checked
    "before:transition-[background-color] before:duration-600"; // transition effect when checked

  const labelCss =
    "flex items-center " + // seems like this is the only way I can get both the checkbox and label vertically aligned
    "hover:font-semibold hover:text-blue-600"; // style when hover

  return (
    <label id={labelId} className={labelCss}>
      <input className={inputCss} type="checkbox" value={value} {...register} />
      {children}
    </label>
  );
}

export function FancyTextInput({
  register,
  placeholder,
  type = "text",
  step,
}: {
  register: any;
  placeholder?: string;
  type?: string;
  step?: string;
}) {
  const inputClass =
    "h-9 border w-full px-2 rounded-md mb-1 " +
    "hover:border-blue-200 hover:border-2 " +
    "focus-visible:outline-none focus-visible:border-blue-600 focus-visible:border-2 focus-visible:shadow-sm focus-visible:shadow-blue-800 " + // focus visible
    "dark:bg-gray-900 " +
    "duration-1000 transition-[box-shadow]";

  return <input className={inputClass} type={type} step={step} {...register()} placeholder={placeholder} />;
}

export function FancyTextArea({ register, placeholder, rows }: { register: any; placeholder?: string; rows?: number }) {
  const inputClass =
    "border w-full px-2 rounded-md mb-1 " +
    "hover:border-blue-200 hover:border-2 " +
    "focus-visible:outline-none focus-visible:border-blue-600 focus-visible:border-2 focus-visible:shadow-sm focus-visible:shadow-blue-800 " + // focus visible
    "dark:bg-gray-900 " +
    "duration-1000 transition-[box-shadow]";

  return <textarea className={inputClass} rows={rows} {...register()} placeholder={placeholder} />;
}

export interface OptionItem {
  label: string;
  value: string | number | string[];
}

export const selectCss =
  "block " +
  "px-3 py-2 " + //
  "rounded border border-[#CCCCCC] " +
  "text-black dark:text-dark-gray-text dark:bg-admin-back-blue  ";
  
export function FancySelect<T>({
  register,
  fieldName,
  registerOptions,
  options,
  className,
  ...restProps
}: {
  register: UseFormRegister<T>;
  fieldName: Path<T>;
  registerOptions?: RegisterOptions<any, any>;
  options: OptionItem[];
  className?: string;
}) {
  return (
    <select {...register(fieldName, registerOptions)} {...restProps} className={`${selectCss} ${className}`}>
      {options?.map((o) => (
        <option key={o.value.toString()} value={o.value}>
          {o.label}
        </option>
      ))}
    </select>
  );
}