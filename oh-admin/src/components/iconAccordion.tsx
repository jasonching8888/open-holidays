export default function Accordion({ isOpen }: { isOpen: boolean }) {
  return (
    <svg
      className={`w-6 h-6 ${isOpen ? "transform rotate-180" : ""}`}
      fill="none"
      viewBox="0 0 24 24"
      stroke="currentColor"
    >
      <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M19 9l-7 7-7-7" />
    </svg>
  );
}
