import Logo from "../../public/logo.png";
import Head from "next/head";
import Image from "next/image";

import MenuBtn from "./menu/menuBtn.comp";
import Nav from "./menu/nav.comp";
import UserBtn from "./menu/useBtn.comp";
import { format } from "date-fns";

export default function Layout({ title, children }) {
  return (
    <>
      <Head>
        <title>{title}</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>

      <header className="bg-white dark:bg-[#373e4a] flex justify-between p-3 items-center space-y-2 shadow">
        {/* <TopLeft /> */}
        <LogoComp />
        <h1>Global Alliance Networks Limited</h1>
        <TopRight />
      </header>

      <div className="min-h-[calc(100vh-72px-24px)] flex text-zinc-500 dark:text-zinc-400 dark:bg-[#181e2b]">
        <Nav className="hidden md:block basis-48 shrink-0 pr-2 dark:bg-[#232b38]" />
        <main className="p-8 container mx-auto">
          <main className="space-y-4">{children}</main>
        </main>
      </div>

      <footer>Copyright©{format(new Date(), "yyyy")} Global Alliance Networks Limited. All rights reserved.</footer>
    </>
  );
}

const TopLeft = () => {
  return (
    <div className="md:hidden flex items-center ml-2">
      <MenuBtn />
    </div>
  );
};

const TopRight = () => {
  return (
    <div className="flex items-center mr-2 my-1">
      <UserBtn />
    </div>
  );
};

function LogoComp() {
  return <Image src={Logo} alt="Logo" unoptimized={true} width={50} />;
}
