import { logout } from "../../core/login.core";

export default function Logout() {
  return (
    <button
      className="border ml-2 bg-blue-200 dark:bg-blue-800 px-2 py-1 rounded-xl mt-3"
      id="logout-button"
      onClick={logout}
    >
      Logout
    </button>
  );
}
