import { Dispatch, MutableRefObject, SetStateAction, useRef, useState } from "react";

import useModalHandler from "../../hooks/useModalHandler.hook";
import MenuIcon from "../../icons/menu.icon";
import Nav from "./nav.comp";

export default function MenuBtn() {
  const [modalOpened, setModalOpened] = useState(false);
  const btnRef = useRef();

  return (
    <>
      <button ref={btnRef} onClick={() => setModalOpened((v) => !v)}>
        <MenuIcon />
      </button>

      {modalOpened && <NavModal setModalOpened={setModalOpened} btnRef={btnRef} />}
    </>
  );
}

const NavModal = ({
  setModalOpened,
  btnRef,
}: {
  setModalOpened: Dispatch<SetStateAction<boolean>>;
  btnRef: MutableRefObject<undefined>;
}) => {
  const modalRef = useModalHandler(btnRef, setModalOpened);

  const className =
    "overflow-y-auto " +
    "p-4 bg-gray-100 dark:bg-gray-800 dark:text-white " +
    "top-16 fixed " + // position
    "w-screen left-0 h-[calc(100%_-_4rem)]"; // mobile layout

  return (
    <div ref={modalRef} className={className}>
      <Nav />
    </div>
  );
};
