import Link from "next/link";

import { useTranslation } from "../../core/translation.core";
import useMeRole from "../../hooks/useMeRole.hook";
import Logout from "./logout.comp";

export default function Nav({ className }: { className?: string }) {
  const { t, msg } = useTranslation();
  const { role, isFetched } = useMeRole();

  if (!isFetched) return null;

  return (
    <nav className={className}>
      <ul>
        <Item id="report" href="/report">
          Report
        </Item>{" "}
        <Item id="coupons" href="/coupons">
          Coupons
        </Item>
        <Item id="restaurants" href="/restaurants">
          Shops
        </Item>
        <Item id="users" href="/users">
          Users
        </Item>
        {/* <Item id="home" href="/report">
          {t(msg.index.dashboard)}
        </Item>
        <Item id="Job" href="/jobs">
          {t(msg.job.job)}
        </Item>
        <Item id="Client" href="/clients">
          {t(msg.client.clients)}
        </Item>
        <Item id="Partner" href="/partners">
          {t(msg.partner.partner)}
        </Item> */}
        {/* <Item id="Employee" href="/employees">
          {t(msg.employee.employees)}
        </Item> */}
        {/* <Item id="Job Nature" href="/jobnatures">
          {t(msg.jobnature.jobnatures)}
        </Item> */}
        {/* <Item id="invoice" href="/invoices">
          Invoices
        </Item>
        {role === "admin" && (
          <Item id="users" href="/users">
            {t(msg.user.user)}
          </Item>
        )} */}
        <Item id="setting" href="/setting">
          {t(msg.setting.setting)}
        </Item>
        <li>
          <Logout />
        </li>
      </ul>
    </nav>
  );
}

const Item = ({ id, href, children }: { id: string; href: string; children: string }) => {
  return (
    <li className="h-10 border-b-2 p-2">
      <Link id={id} href={href}>
        {children}
      </Link>
    </li>
  );
};
