import { useRef, useState } from "react";

import UserIcon from "../../icons/user.icon";
import UserSettingModal from "./userSettingModal.comp";

export default function UserBtn() {
  const [modalOpened, setModalOpened] = useState(false);
  const btnRef = useRef();

  return (
    <>
      <button
        ref={btnRef}
        className="bg-blue-400 rounded-full p-2 text-white hover:bg-blue-500"
        onClick={() => setModalOpened((v) => !v)}
      >
        <UserIcon />
      </button>

      {modalOpened && <UserSettingModal setModalOpened={setModalOpened} btnRef={btnRef} />}
    </>
  );
}
