import { Dispatch, MutableRefObject, SetStateAction } from "react";

import useModalHandler from "../../hooks/useModalHandler.hook";
import { DarkModelToggle } from "../theme.comp";
import Logout from "./logout.comp";

export default function UserSettingModal({
  setModalOpened,
  btnRef,
}: {
  setModalOpened: Dispatch<SetStateAction<boolean>>;
  btnRef: MutableRefObject<undefined>;
}) {
  const modalRef = useModalHandler(btnRef, setModalOpened);

  return (
    <div ref={modalRef}>
      <UserSettingModalDesktop />
      <UserSettingModalMobile />
    </div>
  );
}

const UserSettingModalDesktop = () => {
  const className =
    "hidden md:flex flex-col gap-3 " +
    "p-10 bg-gray-100 dark:bg-gray-800 dark:text-white " +
    "fixed top-16 right-0 " +
    "right-1 rounded-3xl border-2"; // desktop layout
  return (
    <>
      <div id="menu-usersetting-desktop" className={className}>
        <DarkModelToggle />
        <Logout />
      </div>
    </>
  );
};

const UserSettingModalMobile = () => {
  const className =
    "md:hidden " +
    "flex flex-col gap-3 " +
    "p-4 bg-gray-100 dark:bg-gray-800 dark:text-white " +
    "top-16 fixed " + // position
    "w-screen left-0 h-[calc(100%_-_4rem)]"; // mobile layout

  return (
    <>
      <div id="menu-usersetting-desktop" className={className}>
        <DarkModelToggle />
        <Logout />
      </div>
    </>
  );
};
