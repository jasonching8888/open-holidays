import { ErrorMessage } from "@hookform/error-message";
import { ChangeEvent } from "react";
import { UseFormReturn } from "react-hook-form";

import Badges from "./badges.comp";

export default function MultiSelect({
  methods,
  fieldName,
  label,
  options,
}: {
  methods: UseFormReturn<any, any>;
  fieldName: string;
  label: string;
  options: { name: string; value: string }[];
}) {
  function onSelect(e: ChangeEvent<HTMLSelectElement>) {
    let values = methods.getValues(fieldName) as string[];

    if (values === undefined) {
      values = [];
    }

    values.push(e.target.value);
    methods.setValue(fieldName, Array.from(new Set(values)));
  }

  return (
    <div className="my-4">
      <ErrorMessage errors={methods.formState.errors} name="singleErrorInput" />
      <label htmlFor={fieldName} className="block mb-2 text-sm font-medium  dark:text-white">
        {label}
      </label>
      <select
        name={fieldName}
        className="py-3 px-4 pr-9 block  w-full  rounded-md text-sm  dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400"
        aria-label={label}
        onChange={onSelect}
      >
        <option value="">None</option>
        {options.map((item) => (
          <option key={item.value} value={item.value}>
            {item.name}
          </option>
        ))}
      </select>
      <Badges methods={methods} fieldName={fieldName} options={options} />

      <ErrorMessage
        errors={methods.formState.errors}
        name={fieldName}
        render={({ message }) => <div className="text-red-500">{message}</div>}
      />
    </div>
  );
}
