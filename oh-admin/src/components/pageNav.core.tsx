import { Dispatch, SetStateAction } from "react";

import ChevronLeftIcon from "../icons/chevronLeft.icon";
import ChevronRightIcon from "../icons/chevronRight.icon";

interface PageNavProps {
  page: number;
  size: number;
  total: number;
  setPage: Dispatch<SetStateAction<number>>;
}

export default function PageNav({ page, size, total, setPage }: PageNavProps) {
  const pageCount = Math.ceil(total / size);
  const pageArr = [...Array(pageCount).keys()].map((i) => i + 1);

  return (
    <div className="flex gap-2 mt-4 justify-center">
      <NavButton disabled={page === 1} onClick={() => setPage((prev) => prev - 1)}>
        <ChevronLeftIcon />
      </NavButton>
      {pageArr.map((i) => (
        <NavButton key={i} isActive={page === i} onClick={() => setPage(i)}>
          {i}
        </NavButton>
      ))}
      <NavButton disabled={page >= pageCount} onClick={() => setPage((prev) => prev + 1)}>
        <ChevronRightIcon />
      </NavButton>
    </div>
  );
}

interface NavButtonProps {
  children: React.ReactNode;
  isActive?: boolean;
  disabled?: boolean;
  onClick: () => void;
}

const NavButton = ({ children, isActive, disabled = false, onClick }: NavButtonProps) => {
  let className = "w-8 h-8 flex justify-center items-center rounded-full ";

  if (isActive) {
    className += "bg-blue-400 text-white ";
  }

  if (disabled) {
    className += "bg-gray-200 text-white ";
  }

  return (
    <button disabled={disabled} onClick={onClick} className={className}>
      {children}
    </button>
  );
};
