import { FieldPath, UseFormReturn, useWatch } from "react-hook-form";

import ChevronLeftIcon from "../icons/chevronLeft.icon";
import ChevronRightIcon from "../icons/chevronRight.icon";

interface PageNavProps<TFieldValues> {
  pageField: FieldPath<TFieldValues>;
  sizeField: FieldPath<TFieldValues>;
  total: number;
  methods: UseFormReturn<TFieldValues, any>;
}

export default function PageNavForm<TFieldValues>({
  pageField,
  sizeField,
  total,
  methods,
}: PageNavProps<TFieldValues>) {
  const size = useWatch({ name: sizeField, control: methods.control }) as number;
  const page = useWatch({ name: pageField, control: methods.control }) as number;

  const pageCount = Math.ceil(total / size);
  const pageArr = [...Array(pageCount).keys()].map((i) => i + 1);

  return (
    <div className="flex gap-2 mt-4 justify-center">
      <NavButton disabled={page === 1} onClick={() => methods.setValue(pageField, (page - 1) as any)}>
        <ChevronLeftIcon />
      </NavButton>
      {pageArr.map((i) => (
        <NavButton key={i} isActive={page === i} onClick={() => methods.setValue(pageField, i as any)}>
          {i}
        </NavButton>
      ))}
      <NavButton disabled={page >= pageCount} onClick={() => methods.setValue(pageField, (page + 1) as any)}>
        <ChevronRightIcon />
      </NavButton>
    </div>
  );
}

interface NavButtonProps {
  children: React.ReactNode;
  isActive?: boolean;
  disabled?: boolean;
  onClick: () => void;
}

const NavButton = ({ children, isActive, disabled = false, onClick }: NavButtonProps) => {
  let className = "w-8 h-8 flex justify-center items-center rounded-full ";

  if (isActive) {
    className += "bg-blue-400 text-white ";
  }

  if (disabled) {
    className += "bg-gray-200 text-white ";
  }

  return (
    <button disabled={disabled} onClick={onClick} className={className}>
      {children}
    </button>
  );
};
