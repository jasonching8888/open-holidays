import Cookies from "js-cookie";
import { useRouter } from "next/router";

import { useTranslation } from "../core/translation.core";

export default function Locale() {
  const { t, msg } = useTranslation();

  // const { pathname, asPath, query } = useRouter();
  const router = useRouter();

  function changeLocale(value: string) {
    router.push({ pathname: router.pathname, query: router.query }, router.asPath, { locale: value });
    Cookies.set("NEXT_LOCALE", value, { expires: 9999, sameSite: "strict" });
  }

  return (
    <div className="flex gap-4 mt-4">
      <h2>{t(msg.setting.chooseLanguage)}</h2>
      <select
        defaultValue={t(msg.setting.english)}
        id="select-locale"
        className="md:w-1/5 py-3 px-4 pr-9 block w-full border-gray-200 rounded-md text-sm focus:border-blue-500 focus:ring-blue-500 dark:border-gray-700 dark:text-gray-400"
        onChange={(e) => changeLocale(e.target.value)}
        value={router.locale}
      >
        <option value="en">{t(msg.setting.english)}</option>
        <option selected value="zh-Hans">
          {t(msg.setting.chineseS)}
        </option>
        <option value="zh-Hant">{t(msg.setting.chineseT)}</option>
      </select>
    </div>
  );
}
