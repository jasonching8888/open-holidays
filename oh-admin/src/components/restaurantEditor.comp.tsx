import { UseFormReturn } from "react-hook-form";
import { EditingMode, LabelAndInput } from "./form.comp";
import WarningAlert from "./warningAlert.comp";
import SuccessAlert from "./successAlert.comp";
import { useTranslation } from "../core/translation.core";

export default function RestaurantEditor({
  editingMode,
  methods,
  onSubmit,
  jobMutation,
}: {
  editingMode: EditingMode;
  methods: UseFormReturn<RestaurantForm, any>;
  onSubmit: (data: RestaurantForm) => void;
  jobMutation: any;
}) {
  return (
    <div className="flex justify-center ">
      <div className="min-w-full flex flex-col items-center ">
        <h1>Add / Update Restaurant</h1>
        {jobMutation?.isError && <WarningAlert message="Error" />}
        {jobMutation?.isSuccess && (
          <SuccessAlert message={`Job ${editingMode == EditingMode.create ? "added" : "edited"} successfully`} />
        )}

        <form className="flex flex-col items-center min-w-full" onSubmit={methods.handleSubmit(onSubmit)}>
          <div className="md:w-1/2 w-full">
            <LabelAndInput register={methods.register} fieldName="name" label="Name" />
            {/* <LabelAndInput register={methods.register} fieldName="discount" type="number" label="Discount" /> */}
            {/* <RestaurantSelector methods={methods} /> */}
            {/* <FancySelect register={methods.register} fieldName="status" options={
              [
                {value: "enabled", label: "Enabled"},
                {value: "disabled", label: "Disabled"}
              ]
            } /> */}
          </div>

          <SubmitButton editingMode={editingMode} />
        </form>
      </div>
    </div>
  );
}

function SubmitButton({ editingMode }: { editingMode: EditingMode }) {
  const { t, msg } = useTranslation();

  function getSubmitBtnText() {
    switch (editingMode) {
      case EditingMode.create:
        return t(msg.client.add);
      case EditingMode.edit:
        return t(msg.client.edit);
    }
  }

  return (
    <div>
      <input className=" bg-blue-500 text-white m-2 " type="submit" value={getSubmitBtnText()} />
    </div>
  );
}
