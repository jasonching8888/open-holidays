export default function SimpleBadges({ names }: { names: string[] }) {
  return (
    <div className="flex flex-wrap">
      {names.map((n) => (
        <SimpleBadge key={n} name={n} />
      ))}
    </div>
  );
}

function SimpleBadge({ name }: { name: string }) {
  return (
    <div className="inline-block mt-2 px-5 py-2.5 text-sm font-medium text-center text-white bg-blue-700 rounded-lg focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:focus:ring-blue-800">
      {name}
    </div>
  );
}
