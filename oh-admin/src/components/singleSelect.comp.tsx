import { ErrorMessage } from "@hookform/error-message";
import { UseFormReturn } from "react-hook-form";

export default function SingleSelect({
  methods,
  fieldName,
  label,
  options,
  isRequired = true,
}: {
  methods: UseFormReturn<any, any>;
  fieldName: string;
  label: string;
  options: { name: string; value: string }[];
  isRequired?: boolean;
}) {
  return (
    <div className="my-4">
      <ErrorMessage errors={methods.formState.errors} name="singleErrorInput" />
      <label htmlFor={fieldName} className="block mb-2 text-sm font-medium  dark:text-white">
        {label}
      </label>
      <select
        name={fieldName}
        {...methods.register(fieldName, { required: isRequired && "This is required" })}
        className="py-3 px-4 pr-9 block  w-full  rounded-md text-sm  dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400"
        aria-label={label}
      >
        <option value="">None</option>
        {options.map((item) => (
          <option key={item.value} value={item.value}>
            {item.name}
          </option>
        ))}
      </select>
      <ErrorMessage
        errors={methods.formState.errors}
        name={fieldName}
        render={({ message }) => <div className="text-red-500">{message}</div>}
      />
    </div>
  );
}