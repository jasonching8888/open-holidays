import { EditingMode } from "./form.comp";

export default function SubmitButton({ isLoading, editingMode }) {
  let mode;
  switch (editingMode) {
    case EditingMode.create:

      mode = "Add";
      break;
    case EditingMode.edit:
      mode = "Update";
      break;
    default:
      mode = "Apply";
  }

  return (
    <>
      <button
        type="submit"
        disabled={isLoading?true:false}
        className="py-3 mt-2  px-4 inline-flex justify-center items-center gap-2 rounded-md border border-transparent font-semibold bg-blue-500 text-white hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2 transition-all text-sm dark:focus:ring-offset-gray-800"
      >
        <span
          className={`${
            !isLoading ? "hidden" : "inline-block"
          } animate-spin  w-4 h-4 border-[3px] border-current border-t-transparent text-white rounded-full`}
          role="status"
          aria-label="loading"
        ></span>
        {isLoading ? "loading..." : mode}
      </button>
    </>
  );
}
