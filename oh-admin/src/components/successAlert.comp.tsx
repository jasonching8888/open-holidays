import CheckMarkIcon from "../icons/checkMark.icon";

export default function SuccessAlert({ message }) {
  return (
    <div
      id="dismiss-alert"
      className="hs-removing:translate-x-5 hs-removing:opacity-0 transition duration-300 bg-teal-50 border border-teal-200 rounded-md p-4 my-4 md:w-1/2"
      role="alert"
    >
      <div className="flex">
        <div className="flex-shrink-0">
          <CheckMarkIcon />
        </div>
        <div className="ml-3">
          <div className="text-sm text-teal-800 font-medium">{message}</div>
        </div>
      </div>
    </div>
  );
}
