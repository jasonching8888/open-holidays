import moment from "moment";
import { ReactNode } from "react";

import { formatAmt } from "../core/format.core";
import { tableHeadRow } from "../styles/table.css";

export function TdAmt({ children }: { children: number }) {
  return (
    <td className="border border-slate-700 px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-800 dark:text-gray-200 text-right">
      {formatAmt(children)}
    </td>
  );
}

export function Td({ children }: { children: ReactNode }) {
  return (
    <td className="border border-slate-700 px-2 py-2 whitespace-nowrap text-sm font-medium text-gray-800 dark:text-gray-200">
      {children}
    </td>
  );
}

export function TdDate({ date }: { date: Date }) {
  return <Td>{date ? moment(date).format("YYYY-MM-DD") : null}</Td>;
}

export const Th = ({ children }: { children: React.ReactNode }) => {
  return (
    <th scope="col" className={tableHeadRow}>
      {children}
    </th>
  );
};
