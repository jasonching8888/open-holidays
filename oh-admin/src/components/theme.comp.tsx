import { useLocalStorage, writeStorage } from "@rehooks/local-storage";
import { useEffect } from "react";

import { useTranslation } from "../core/translation.core";
import Toggle from "./toggle.core";

// Add class "dark" to <html> tag if it's dark mode
// This hook should be added to the _app.tsx only
// https://tailwindcss.com/docs/dark-mode
export function useDarkModeSync() {
  const [theme] = useLocalStorage("theme");

  useEffect(() => {
    if (theme === "dark" || (!theme && window.matchMedia("(prefers-color-scheme: dark)").matches)) {
      document.documentElement.classList.add("dark");
    } else {
      document.documentElement.classList.remove("dark");
    }
  }, [theme]);
}

// A simple hook that return the isDark boolean
function useDarkMode() {
  const [theme] = useLocalStorage("theme");

  if (typeof window === "undefined") return false;

  if (theme) {
    return theme === "dark";
  }

  return window.matchMedia("(prefers-color-scheme: dark)").matches;
}

export function DarkModelToggle() {
  const isDark = useDarkMode();
  const { t, msg } = useTranslation();

  return (
    <div>
      <p className="text-xs mb-2">{t(msg.menu.colorTheme)}</p>
      <Toggle
        title={t(msg.menu.darkMode)}
        isChecked={isDark}
        setIsChecked={() => {}}
        onChange={(isChecked) => {
          writeStorage("theme", isChecked ? "dark" : "light");
        }}
      />
    </div>
  );
}
