import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useState } from "react";

import { patch } from "../core/fetch.core";

export default function ToggleButton({ status, url, queryKey, id }) {
  const queryClient = useQueryClient();
  const disableUser = useMutation({
    mutationFn: async (data: { url: string; id: string; disable: boolean }) =>
      await patch(data.url, data.id, { isDisable: data.disable }),
    onSuccess: (data) => {
      if (data.status === 200) {
        queryClient.invalidateQueries({queryKey: [queryKey] });
      }
    },
  });
  const isDisable = status !== "created" ? true : false;

  const handleToggle = async (e) => {
    e.preventDefault();
    const data = await disableUser.mutate({ url, id, disable: isDisable });
    console.log(data);
  };

  return (
    <>
      <label className="relative inline-flex items-center cursor-pointer">
        <input
          type="checkbox"
          onChange={handleToggle}
          value=""
          className="sr-only peer"
          checked={status !== "created" ? true : false}
        />
        <div className="w-11 h-6 bg-gray-200 rounded-full peer peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 dark:bg-gray-700 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:left-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600"></div>
        <span className="ml-3 text-sm font-medium text-gray-900 dark:text-gray-300">Disable User</span>
      </label>
    </>
  );
}
