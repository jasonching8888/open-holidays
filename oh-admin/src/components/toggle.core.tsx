//  Design Reference: https://flowbite.com/docs/forms/toggle/

import { Dispatch, SetStateAction, useEffect, useRef } from "react";

interface Props {
  title: string;
  isChecked: boolean;
  setIsChecked: Dispatch<SetStateAction<boolean>>;

  // I was thinking about to just sync it with isChecked state, then use useEffect to watch the change
  // But then I realiase that it's convinence to know if the change is from user interaction or system
  // so I create this seperate callback
  onChange: (isChecked: boolean) => void;
}

export default function Toggle(props: Props) {
  const inputRef = useRef();

  useEffect(() => {
    if (!inputRef.current) return;

    if ((inputRef.current as any).checked !== props.isChecked) {
      (inputRef.current as any).checked = props.isChecked;
    }
  }, [props.isChecked, inputRef]);

  return (
    <label className="relative inline-flex items-center cursor-pointer">
      <input
        ref={inputRef}
        type="checkbox"
        className="sr-only peer"
        onChange={(e) => {
          props.onChange(e.target.checked);
          props.setIsChecked(e.target.checked);
        }}
      />
      <div className="w-11 h-6 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:left-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600"></div>
      <span className="ml-3 font-medium">{props.title}</span>
    </label>
  );
}
