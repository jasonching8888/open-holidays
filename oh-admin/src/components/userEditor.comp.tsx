import { useEffect } from "react";
import { useForm } from "react-hook-form";

import { useTranslation } from "../core/translation.core";
// import { AdminUser, AdminUserFormData } from "../types/user";
import { FancyTextInput, LabelAndInput } from "./form.comp";

interface Props {
  user?: User;
  onSubmit: (uesr: AdminUserFormData) => void;
}

export default function AdminUserEditor({ user, onSubmit }: Props) {
  const { register, handleSubmit, reset } = useForm<AdminUserFormData>();
  const { t, msg } = useTranslation();

  useEffect(() => {
    if (user) {
      reset(convertUserToForm(user));
    }

    // eslint-disable-next-line
  }, [user]);

  function isCreateMode() {
    return user === null || user === undefined;
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <input className="h-9 border w-full" name="name" type="hidden" {...register("id")} />
      <LabelAndInput type="text" register={register} fieldName="name" label={t(msg.user.name)} />
      <LabelAndInput type="email" register={register} fieldName="email" label={t(msg.user.email)} />

      <label>
        {t(msg.user.role)}
        <select className="h-9 border w-full" name="role" {...register("role")}>
          <option id="regular" value="regular">
            Regular
          </option>
          <option id="admin" value="admin">
            Admin
          </option>
        </select>
      </label>

      <SubmitButton isCreateMode={isCreateMode()} />
    </form>
  );
}

const SubmitButton = ({ isCreateMode }: { isCreateMode: boolean }) => {
  const { t, msg } = useTranslation();

  function getSubmitBtnText() {
    return isCreateMode ? t(msg.user.addUser) : t(msg.user.edit);
  }

  return (
    <div>
      <input className="mt-4" type="submit" value={getSubmitBtnText()} />
    </div>
  );
};

function convertUserToForm(user: User): AdminUserFormData {
  return null;
  // return {
  //   email: user.email,
  //   id: user._id,
  //   name: user.name,
  //   role: user.role,
  //   status: user.status,
  // };
}
