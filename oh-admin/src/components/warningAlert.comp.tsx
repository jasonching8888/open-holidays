import AlertIcon from "../icons/altert.icon";

export default function WarningAlert({ message }) {
  return (
    <div className=" bg-white border rounded-md shadow-lg dark:border-gray-700 md:w-1/2 my-4" role="alert">
      <div className="flex p-4">
        <div className="flex-shrink-0">
          <AlertIcon />
        </div>
        <div className="ml-3">
          <p className="text-sm text-gray-700 dark:text-gray-400">{message}</p>
        </div>
      </div>
    </div>
  );
}
