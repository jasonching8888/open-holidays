import { get, patch, post } from "./fetch.core";

export async function couponList() {
  const res = await get<Coupon[]>("/admin/coupon/list");
  return res.data;
}

export async function getCoupon(id: string): Promise<Coupon> {
  const coupons = await couponList();
  return coupons.find((c) => c.id === id);
}

export async function createCoupon(data: CouponForm) {
  await post("/admin/coupon/create", data);
}

export async function updateCoupon(id: string, data: CouponForm) {
  await post("/admin/coupon/edit", {
    id,
    ...data,
  });
}

export async function getRedeemReport() {
  const res = await get<ReportCoupon[]>("/admin/coupon/reportRedeem");
  return res.data;
}
