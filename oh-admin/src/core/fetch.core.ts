import { getLoginToken, removeLoginToken } from "./login.core";

function getHeader(option: { useToken: boolean; useShopId: boolean; additional?: { [key: string]: string } }) {
  var headers = {
    ...option.additional,
  };

  if (option.useToken && getLoginToken()) {
    headers["x-access-token"] = getLoginToken();
  }

  return headers;
}

export async function get<T>(url: string, option = { useToken: true, useShopId: true }) {
  const res = await fetch(process.env.NEXT_PUBLIC_URL_ROOT + url, {
    headers: getHeader({ useToken: option.useToken, useShopId: option.useShopId }),
  });

  if (res.status !== 200) {
    const errorContent = await res.json();

    if (res.status === 401 && errorContent.message && errorContent.message === "invalidToken") {
      removeLoginToken();
    }

    throw new Error(`Fetch Failed with url '${url}': ${JSON.stringify(errorContent)}`);
  }

  return {
    status: res.status,
    data: (await res.json()) as T,
  };
}

export async function post<T>(url: string, data: any, option = { useToken: true, useShopId: true }) {
  const res = await fetch(process.env.NEXT_PUBLIC_URL_ROOT + url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "x-access-token": option.useToken ? getLoginToken() : undefined,
    },
    body: JSON.stringify(data),
  });
  if (res.status === 200 || res.status === 201) {
    return {
      status: res.status,
      data: (await res.json()) as T,
    };
  }

  const resData = await res.json();
  throw new Error(`Fetch Failed with url '${url}': ${JSON.stringify(resData)}`);
}

export async function postFormData<T>(url: string, data: FormData, option = { useToken: true, useShopId: true }) {
  // console.log("postFormData", data);

  // const res = await axios.postForm(process.env.NEXT_PUBLIC_URL_ROOT + url, data, {
  //   headers: {
  //     "x-access-token": option.useToken ? getLoginToken() : undefined,
  //     "shop-id": option.useShopId ? getShopId() : undefined,
  //   },
  // });

  // return {
  //   status: res.status,
  //   data: res.data,
  // };

  const res = await fetch(process.env.NEXT_PUBLIC_URL_ROOT + url, {
    method: "POST",
    headers: {
      "x-access-token": option.useToken ? getLoginToken() : undefined,
    },
    body: data,
  });

  if (res.status !== 200) {
    const data = await res.json();
    console.error(data);
    throw new Error("Fetch Failed:" + JSON.stringify(data));
  }

  return {
    status: res.status,
    data: (await res.json()) as T,
  };
}

export async function deleteAction<T>(url: string, id: string, option = { useToken: true, useShopId: true }) {
  const res = await fetch(`${process.env.NEXT_PUBLIC_URL_ROOT}${url}${id}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      "x-access-token": option.useToken ? getLoginToken() : undefined,
    },
  });

  if (res.status !== 200) {
    const data = await res.json();
    throw new Error(`Fetch Failed with url '${url}': ${JSON.stringify(data)}`);
  }

  return {
    status: res.status,
    data: (await res.json()) as T,
  };
}

export async function patch<T>(url: string, id: string, data: any, option = { useToken: true, useShopId: true }) {
  const res = await fetch(process.env.NEXT_PUBLIC_URL_ROOT + url + id, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
      "x-access-token": option.useToken ? getLoginToken() : undefined,
    },
    body: JSON.stringify(data),
  });
  if (res.status !== 200) {
    const data = await res.json();
    throw new Error(`Fetch Failed with url '${url}': ${JSON.stringify(data)}`);
  }

  return {
    status: res.status,
    data: (await res.json()) as T,
  };
}
