const nf = new Intl.NumberFormat("en-US");

export function formatAmt(amt: number) {
  return nf.format(amt);
}
