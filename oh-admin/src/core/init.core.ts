import Router from "next/router";

import { forceLogin, getLoginToken, removeLoginToken } from "./login.core";
import profileCore from "./profile.core";

async function performInit(): Promise<void> {
  if (window.location.pathname.startsWith("/user/verify")) return;

  forceLogin();

  if (getLoginToken() !== undefined) {
    try {
      await profileCore.getMe();
    } catch (e) {
      removeLoginToken();
      Router.push("/login");
    }
  }
}

export let initPromise: Promise<void>;

export default function init() {
  initPromise = performInit();
}
