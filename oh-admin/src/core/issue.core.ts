import { get, post } from "./fetch.core";

export async function listIssue(couponId: string) {
  const res = await get<Redeem[]>(`/admin/coupon/listIssue/${couponId}`);
  return res.data;
}

export async function issue(couponId: string, userId: string) {
  await post("/admin/coupon/issue", {
    couponId,
    userId,
  });
}
