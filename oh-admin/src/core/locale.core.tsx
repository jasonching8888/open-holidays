import Router from "next/router";
import { ReactNode } from "react";

import { LocaleText } from "../types/locale";

function getLocalValuesInPs(value: LocaleText, availableLocales: string[]) {
  var nodes = [] as ReactNode[];

  for (const locale of availableLocales) {
    nodes.push(<p data-locale={locale}>{getLocaleValue(value, locale)}</p>);
  }

  return nodes;
}

function getLocaleValue(value: LocaleText, locale: string) {
  if (!value) return undefined;

  switch (locale) {
    case "en":
      return value.en;
    case "zh-Hant":
      return value.zhHant;
    case "zh_Hans":
      return value.zhHans;
    default:
      return value.en;
  }
}

function isNumber(value: any) {
  if (isNaN(value)) {
    return false;
  } else {
    return true;
  }
}

function getLocaleValueDefault(value: LocaleText) {
  return getLocaleValue(value, Router.locale);
}

function getHKD(value: number) {
  const formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "HKD",
  });

  return formatter.format(value);
}

function isEmpty(value: LocaleText) {
  if (!value) return false;
  if (!value.en) return false;
  if (!value.zhHans) return false;
  if (!value.zhHant) return false;
  return true;
}

const obj = {
  getLocalValuesInPs,
  getLocaleValue,
  getLocaleValueDefault,
  isEmpty,
  getHKD,
  isNumber,
};

export default obj;
