import Cookies from "js-cookie";
import Router from "next/router";

export function getLoginToken() {
  return Cookies.get("loginTokenAdmin");
}

export function removeLoginToken() {
  Cookies.remove("loginTokenAdmin");
}

export function forceLogin() {
  if (getLoginToken() === undefined) {
    Router.push("/login");
  }
}

export function redirectIfLogined() {
  if (getLoginToken() !== undefined) {
    Router.push("/coupons");
  }
}

export async function performLogin(data: any) {
  if (!(await login(data))) {
    return false;
  }

  Router.push("/coupons");
}

async function login(data: any) {
  let response = await fetch(process.env.NEXT_PUBLIC_URL_ROOT + "/admin/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });

  const content = await response.json();

  if (response.status !== 200) {
    return false;
  }

  Cookies.set("loginTokenAdmin", content.token, { expires: 7, sameSite: "strict" });
  return true;
}

export async function logout() {
  Cookies.remove("loginTokenAdmin");
  Cookies.remove("shopId");

  Router.push("/login");
}
