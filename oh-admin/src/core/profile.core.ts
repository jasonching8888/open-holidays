import { Me } from "../types/profile";
import { get } from "./fetch.core";

async function getMe() {
  const res = await get<Me>("/admin/me");
  return res.data;
}

const obj = { getMe };

export default obj;
