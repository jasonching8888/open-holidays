import { get, post } from "./fetch.core";

export async function listRedeem(couponId: string) {
  const res = await get<Redeem[]>(`/admin/coupon/listRedeem/${couponId}`);
  return res.data;
}

export async function redeem(couponId: string, userId: string) {
  await post("/admin/coupon/redeem", {
    couponId,
    userId,
  });
}
