import { get, post } from "./fetch.core";

export async function getRestaurantList() {
  const res = await get<Restaurant[]>("/admin/restaurant/list");
  return res.data;
}

export async function getRestaurant(id: string) {
  const list = await getRestaurantList();
  return list.find((l) => l.id === id);
}

export async function createRestaurant(data: RestaurantForm) {
  await post("/admin/restaurant/create", data);
}

export async function editRestaurant(id: string, data: RestaurantForm) {
  await post(`/admin/restaurant/edit`, { id, ...data });
}
