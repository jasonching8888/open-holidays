import { removeLoginToken } from "./login.core";

export async function serverGet(route: string, token: string) {
  const res = await fetch(process.env.NEXT_PUBLIC_URL_ROOT + route, {
    headers: {
      "Content-Type": "application/json",
      "x-access-token": token,
    },
  });
  if (res.status !== 200) {
    const errorContent = await res.json();

    if (res.status === 401 && errorContent.message && errorContent.message === "invalidToken") {
      removeLoginToken();
    }

    throw new Error(`Fetch Failed with url '${route}': ${JSON.stringify(errorContent)}`);
  }

  return {
    status: res.status,
    data: await res.json(),
  };
}
