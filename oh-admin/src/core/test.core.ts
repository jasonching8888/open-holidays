import { post } from "./fetch.core";

async function clone100Products() {
  await post("/test/clone100Products?specialCode=wERKJFDkewdso34S!8s", {});
}

const obj = {
  clone100Products,
};

export default obj;
