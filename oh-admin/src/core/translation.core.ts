import { useRouter } from "next/router";

import messages, { TransMsg } from "../i18n/messages";

function getMessage(msg: TransMsg, locale: string): string {
  if (locale === undefined) return msg["en"];
  return msg[locale.replaceAll("-", "")];
}

export function useTranslation() {
  const { locale } = useRouter();

  const t = (msg: TransMsg) => {
    return getMessage(msg, locale);
  };

  return { msg: messages, t };
}
