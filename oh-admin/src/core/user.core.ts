// import { AdminUserFormData, AdminUserRole, AdminUserStatus, User } from "../types/user";
import { get, post } from "./fetch.core";

export enum VerifyStatus {
  failedOrNotExist,
  alreadyVerified,
  pending,
}

function getRoleText(role: AdminUserRole) {
  switch (role) {
    case "regular":
      return "Regular";
    case "admin":
      return "Admin";
  }
}

function getStatusText(status: AdminUserStatus) {
  switch (status) {
    case "pending":
      return "Pending";
    case "created":
      return "Created";
    case "deactivated":
      return "Deactivated";
  }
}

async function getUser(id: string) {
  const res = await get<User>(`/admin/user/get?adminUserId=${id}`);
  return res.data;
}

async function updateUser(data: AdminUserFormData) {
  await post("/admin/user/update", {
    id: data.id,
    email: data.email,
    role: data.role,
    name: data.name,
  });
}

export async function listUsers() {
  const res = await get<User[]>("/admin/user/list");
  return res.data;
}

async function addUser(user: AdminUserFormData) {
  await post("/admin/user/add", {
    email: user.email,
    name: user.name,
    role: user.role,
  });
}

// async function loadAdminUsers() {
//   let res = await get<AdminUser[]>("/admin/user/list");
//   return res.data;
// }

async function verifyUserStatus(userId: string, verificationCode: string) {
  const res = await get<{ status: VerifyStatus }>(
    `/admin/verifyUserStatus?userId=${userId}&emailVerificationCode=${verificationCode})`,
  );
  return res.data.status;
}

async function verifyUser(userId: string, emailVerificationCode: string, password: string) {
  await post("/admin/verifyUser", {
    userId,
    emailVerificationCode,
    password,
  });
}

async function resendInvitation(userId: string) {
  await post("/admin/user/resendInvitation", {
    id: userId,
  });
}

const obj = {
  getRoleText,
  getStatusText,
  addUser,
  verifyUserStatus,
  verifyUser,
  getUser,
  updateUser,
  resendInvitation,
};
export default obj;
