import { useQuery } from "@tanstack/react-query";
import { useRouter } from "next/router";
import { getCoupon } from "../core/coupon.core";

export default function useCoupon() {
  const { query, isReady } = useRouter();

  return useQuery({
    queryKey: ["coupons", query.id],
    queryFn: async (context) => getCoupon(context.queryKey[1] as string),
    enabled: isReady,
  });
}
