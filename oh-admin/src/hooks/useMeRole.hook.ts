import { useQuery } from "@tanstack/react-query";

import profileCore from "../core/profile.core";

export default function useMeRole() {
  const { data, isFetched } = useQuery({
    queryKey: ["me"],
    queryFn: profileCore.getMe,
  });
  return {
    role: isFetched ? data?.role : null,
    isFetched,
  };
}
