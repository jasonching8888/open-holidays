import { Dispatch, MutableRefObject, SetStateAction, useEffect, useRef } from "react";

export default function useModalHandler(
  btnRef: MutableRefObject<undefined>,
  setModalOpened: Dispatch<SetStateAction<boolean>>
) {
  const modalRef = useRef<null>();

  useEffect(() => {
    const handleClick = (e: MouseEvent) => {
      if (!modalRef || !modalRef.current) return;

      const container = modalRef.current as any;

      // the btn click toggle will take care of this
      // skip this from this event
      const isBtn = (btnRef.current as any).contains(e.target);
      const isInside = container.contains(e.target);

      if (!isInside && !isBtn) {
        setModalOpened(false);
      }
    };

    window.addEventListener("mouseup", handleClick);

    return () => window.removeEventListener("mouseup", handleClick);
  }, [modalRef, setModalOpened, btnRef]);

  return modalRef;
}
