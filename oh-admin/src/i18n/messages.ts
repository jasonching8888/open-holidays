import { cpSync } from "fs";

export interface TransMsg {
  en: string;
  zhHant: string;
  zhHans: string;
}

function build(...text): TransMsg {
  return {
    en: text[0],
    zhHant: text[1],
    zhHans: text[2],
  };
}

const messages = {
  index: {
    dashboard: build("Dashboard", "儀表版", "儀表版"),
  },
  menu: {
    logout: build("Logout", "登出", "登出"),
    changeLang: build("Change Language", "更改語言", "更改语言"),
    darkMode: build("Dark Mode", "黑暗模式", "黑暗模式"),
    colorTheme: build("Color Theme", "顏色主題", "颜色主题"),
  },
  product: {
    product: build("Product", "產品", "產品"),
    products: build("Products", "產品", "產品"),
    add: build("Add Product", "新增產品", "新增產品"),
    name: build("Product Name", "產品名稱", "產品名稱"),
    description: build("Product Description", "產品說明", "產品說明"),
    price: build("Product Price", "產品價格", "產品價格"),
    edit: build("Edit Product", "更改產品", "更改產品"),
    productGallery: build("Product Gallery", "產品圖庫", "產品圖庫"),
    featuredImage: build("Featured Image", "主要圖片", "主要圖片"),
    descriptionGallery: build("Description Gallery", "描述圖庫", "描述圖庫"),
    sku: build("SKU", "產品編號", "產品編號"),
    isPromotionProduct: build("Promotion Product", "推廣產品", "推廣產品"),
    simple: build("Simple", "簡易", "簡易"),
    variationParent: build("Variation (Parent)", "多類 (母)", "多類 (母)"),
    variationChild: build("Variation (Child)", "多類 (子)", "多類 (子)"),
    createChildProduct: build("Create Variation Product (Child)", "更改多類 (子) 產品", "更改多類 (子) 產品"),
    editChildProduct: build("Edit Variation Product (Child)", "更改多類 (子) 產品", "更改多類 (子) 產品"),
    addMore: build("Add More", "新增", "新增"),
    submit: build("Submit", "提交", "提交"),
    addChild: build("Add Child", "新增子產品", "新增子產品"),
  },
  category: {
    category: build("Category", "類別", "類別"),
    add: build("Add Category", "新增類別", "新增類別"),
    name: build("Category Name", "類別名稱", "類別名稱"),
    slug: build("Slug", "路徑標籤", "路徑標籤"),
    parent: build("Parent Category", "上層類別", "上層類別"),
  },
  attribute: {
    attribute: build("Attribute", "屬性", "屬性"),
    add: build("Add Attribute", "新增屬性", "新增屬性"),
    edit: build("Edit Attribute", "更改屬性", "更改屬性"),
    name: build("Attribute Name", "屬性名稱", "屬性名稱"),
    type: build("Attribute Type", "屬性類形", "屬性類形"),
    unit: build("Attribute Unit", "屬性單位", "屬性單位"),
    create: build("Create", "新增", "新增"),
    update: build("Update", "更新", "更新"),
    text: build("Text", "文字", "文字"),
    number: build("Number", "數值", "數值"),
  },
  merchant: {
    merchants: build("Merchants", "分銷商", "分銷商"),
    create: build("Create New Merchant", "新增分銷商", "新增分銷商"),
    addUser: build("Add Merchant User", "新增分銷商用戶", "新增分銷商用戶"),
    typeAdmin: build("Admin", "管理員", "管理員"),
    typeShopManager: build("Shop Manager", "商店管理員", "商店管理員"),
  },
  customer: {
    customer: build("Customer", "客戶", "客戶"),
  },
  media: {
    media: build("Media", "媒體", "媒體"),
    add: build("Add Media File", "新增媒體", "新增媒體"),
    addS: build("Add Media Files", "新增媒體", "新增媒體"),
  },
  setting: {
    setting: build("Profile Setting", "個人設定", "個人設定"),
    chooseLanguage: build("Choose Language", "選擇語言", "選擇語言"),
    chooseShop: build("Choose Shop", "選擇商店", "選擇商店"),
    languages: build("Languages", "語言", "語言"),
    english: build("English", "英文", "英文"),
    chineseT: build("Traditional Chinese", "繁體中文", "繁體中文"),
    chineseS: build("Simplified Chinese", "簡體中文", "簡體中文"),
    userRole: build("User Role", "用戶角色", "用戶角色"),
    email: build("Email Address", "電郵地址", "電郵地址"),
    defaultLocale: build("Default Language", "默認語言", "默認語言"),
    currencies: build("Currencies", "貨幣", "貨幣"),
    defaultCurrency: build("Default Currency", "點認貨幣", "點認貨幣"),
    hkd: build("HKD", "港幣", "港幣"),
    cny: build("CNY", "人民幣", "人民幣"),
    usd: build("USD", "美金", "美金"),
  },
  user: {
    user: build("User", "用戶", "用戶"),
    users: build("Users", "用戶", "用戶"),
    addUser: build("Add User", "新增用戶", "新增用戶"),
    resend: build("Resend", "重新發送", "重新發送"),
    resendStatusFailed: build("Failed", "發送失敗", "發送失敗"),
    resendStatusSending: build("Sending...", "發送中", "發送中"),
    resendStatusSent: build("Sent", "發送成功", "發送成功"),
    edit: build("Edit", "Edit", "Edit"),
    name: build("Name", "name", "name"),
    email: build("Email", "email", "email"),
    role: build("Role", "role", "role"),
    status: build("Status", "status", "status"),
  },
  shop: {
    setting: build("Shop Setting", "商店設定", "商店設定"),
    name: build("Shop Name", "商店名稱", "商店名稱"),
    rootUrl: build("Root URL", "根網址", "根網址"),
    emailFrom: build("Email From", "電郵 From", "電郵 From"),
    smtp: build("SMTP", "SMTP", "SMTP"),
    logo: build("Logo", "Logo", "Logo"),
    googleClientId: build("Google Client ID", "Google Client ID", "Google Client ID"),
    googleSecret: build("Google Secret", "Google Secret", "Google Secret"),
    submit: build("Submit", "提交", "提交"),
    createShop: build("Create Shop", "建立商店", "建立商店"),
    shopTypeRegular: build("Regular Shop", "一般商店", "一般商店"),
    shopTypeMultiMerchant: build("Multi Merchant Shop", "多戶式商店", "多戶式商店"),
    shopType: build("Shop Type", "商店類型", "商店類型"),
  },
  language: {
    en: build("English", "英文", "英文"),
    zhHant: build("Chinese (Traditional)", "繁體中文", "繁體中文"),
    zhHans: build("Chinese (Simplified)", "簡體中文", "簡體中文"),
  },
  pickupAddr: {
    pickupAddr: build("Store Pickup Address", "自取點地址", "自取點地址"),
    addNew: build("Add New Address", "新增自取點", "新增自取點"),
    add: build("Add", "新增", "新增"),
    update: build("Update", "更新", "更新"),
    address: build("Address", "地址", "地址"),
    openingHour: build("Opening Hour", "營業時間", "營業時間"),
    tel: build("Phone Number", "電話號碼", "電話號碼"),
  },

  job: {
    job: build("Jobs", "Job zhHant", "Job zhHans"),
    add: build("Add Job", "Add zhHant", "Add zhHans"),
    name: build("Job Name", "Name zhHant", "Name zhHans"),
    slug: build("Slug", "Slug zhHant", "Slug zhHans"),
  },

  partner: {
    partner: build("Partner", "Partner zhHant", "Partner zhHans"),
    name: build("Name", "name zhHant", "name zhHans"),
    add: build("Add", "add zhHant", "add zhHans"),
    edit: build("edit", "edit zhHant", "edit zhHans"),
  },

  employee: {
    employee: build("Employee", "Employee zhHant", "Employee zhHans"),
    employees: build("Employees", "Employees zhHant", "Employees zhHans"),
  },

  jobnature: {
    jobnature: build("Job Nature", "Job Nature zhHant", "Job Nature zhHans"),
    jobnatures: build("Job Natures", "Job Natures zhHant", "Jobs Nature zhHans"),
  },

  client: {
    client: build("Client ", "Client  zhHant", "Client  zhHans"),
    clients: build("Clients ", "Clients zhHant", "Clients zhHans"),
    createdAt: build("CreatedAt ", "CreatedAt zhHant", "CreatedAt zhHans"),
    stockCode: build("StockCode ", "StockCode zhHant", "StockCode zhHans"),
    name: build("Name", "Name zhHant", "Name zhHans"),
    add: build("Add", "Add zhHant", "Add zhHans"),
    edit: build("Edit", "edit zhHant", "edit zhHans"),
  },
};

export default messages;
