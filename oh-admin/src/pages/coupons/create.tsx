import { useForm } from "react-hook-form";
import CouponEditor from "../../components/couponEditor.comp";
import { EditingMode } from "../../components/form.comp";
import Layout from "../../components/layout.comp";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { createCoupon } from "../../core/coupon.core";
import { useRouter } from "next/router";

export default function CreateCouponPage() {
  const methods = useForm<CouponForm>();
  const { push } = useRouter();
  const queryClient = useQueryClient();

  const jobMutation = useMutation({
    mutationFn: async (data: CouponForm) => createCoupon(data),
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ["coupons"] });
      push("/coupons");
    },
  });

  return (
    <Layout title="Add Coupon">
      <CouponEditor
        editingMode={EditingMode.create}
        methods={methods}
        onSubmit={(data) => jobMutation.mutateAsync(data)}
        jobMutation={jobMutation}
      />
    </Layout>
  );
}
