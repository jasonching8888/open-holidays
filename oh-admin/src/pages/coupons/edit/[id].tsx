import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { useForm } from "react-hook-form";

import CouponEditor from "../../../components/couponEditor.comp";
import { EditingMode } from "../../../components/form.comp";
import Layout from "../../../components/layout.comp";
import { updateCoupon } from "../../../core/coupon.core";
import useCoupon from "../../../hooks/useCoupon.hook";

export default function CouponEditPage() {
  return (
    <Layout title="Edit Coupon">
      <Editor />
    </Layout>
  );
}

const Editor = () => {
  const methods = useForm<CouponForm>();
  const { push, query, isReady } = useRouter();
  const queryClient = useQueryClient();

  const { data: coupon } = useCoupon();

  useEffect(() => {
    if (coupon) {
      methods.reset({
        id: coupon.id,
        code: coupon.code,
        discount: coupon.discount,
        usageLimit: coupon.usageLimit,
        status: coupon.status,
        restaurantId: coupon.restaurant.id,
      });
    }

    // eslint-disable-next-line
  }, [coupon]);

  const jobMutation = useMutation({
    mutationFn: async (data: CouponForm) => updateCoupon(query.id as string, data),
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ["coupons"] });
      push("/coupons");
    },
  });

  return (
    <CouponEditor
      editingMode={EditingMode.edit}
      methods={methods}
      onSubmit={(data) => jobMutation.mutateAsync(data)}
      jobMutation={jobMutation}
    />
  );
};
