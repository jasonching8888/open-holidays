import { useQuery } from "@tanstack/react-query";
import Link from "next/link";
import { UseFormReturn, useForm, useWatch } from "react-hook-form";

import Dialog from "../../components/dialog.comp";
import Layout from "../../components/layout.comp";
import Spinner from "../../components/spinner.comp";
import { Td } from "../../components/table.comp";
import { couponList } from "../../core/coupon.core";
import { table, tableBody, tableHead, tableHeadRow } from "../../styles/table.css";
import { FilterForm } from "../../types/filter";

export default function CouponListPage() {
  const methods = useForm<FilterForm>({
    defaultValues: {
      paging: { size: 50, page: 1 },
      sort: "asc",
      status: "",
      clientId: "",
      range: "",
      workingPartnerIds: "",
      referralPartnerIds: "",
      to: null,
      from: null,
      jobNatureId: "",
    },
  });
  const filterValues = useWatch({ control: methods.control });

  const {
    data: coupons,
    isLoading,
    isError,
    isFetching,
  } = useQuery({
    queryKey: ["coupons"],
    queryFn: () => couponList(),
  });

  if (isError) {
    return <h1>Fetching Error</h1>;
  }

  if (isLoading) {
    return <Spinner />;
  }

  return (
    <Layout title="">
      <div className="flex flex-col">
        <div className="flex flex-row justify-between justify-items-center  ">
          <h1>Coupons</h1>
          <ul className="flex flex-col ">
            <li className="inline-flex items-center cursor-pointer gap-x-2.5 py-3 px-4 text-sm font-medium bg-white border text-gray-800 -mt-px first:rounded-t-lg first:mt-0 last:rounded-b-lg sm:-ml-px sm:mt-0 sm:first:rounded-tr-none sm:first:rounded-bl-lg sm:last:rounded-bl-none sm:last:rounded-tr-lg dark:bg-gray-800 dark:border-gray-700 dark:text-white ">
              <Link className="hover:text-blue-600 " href="/coupons/create">
                Add Coupon
              </Link>
            </li>
          </ul>
        </div>
        {/* <div className="flex justify-end mt-3">
          <ExcelExportArray<FilterForm> filterValues={filterValues} getArray={cleanJobs} name="jobs" />
        </div> */}
        <div className="flex flex-col gap-4">
          <CouponList isFetching={isFetching} coupons={coupons} methods={methods} />
        </div>
      </div>
    </Layout>
  );
}

const CouponList = ({
  coupons,
  isFetching,
  methods,
}: {
  coupons: Coupon[];
  isFetching: boolean;
  methods: UseFormReturn<FilterForm, any>;
}) => {
  if (isFetching)
    return (
      <div className="overflow-x-auto w-full md:w-4/05">
        <Spinner />
      </div>
    );

  if (coupons.length === 0)
    return <div className="overflow-x-auto w-full md:w-4/05 flex justify-center text-lg">No Records Found</div>;

  return (
    <>
      <div className="overflow-x-auto w-full md:w-4/05">
        <table className={table}>
          <thead className={tableHead}>
            <tr>
              <Th>Coupon Code</Th>
              <Th>Restaurant</Th>
              <Th>Redeem Count</Th>
              <Th>Issue Count</Th>
              <Th> </Th>
            </tr>
          </thead>
          <tbody className={tableBody}>
            {coupons.map((coupon) => (
              <Row key={coupon.id} coupon={coupon} />
            ))}
          </tbody>
        </table>
      </div>

      {/* <PageNavForm pageField="paging.page" sizeField="paging.size" total={jobRes.total} methods={methods} /> */}
    </>
  );
};

const Th = ({ children }: { children: React.ReactNode }) => {
  return (
    <th scope="col" className={tableHeadRow}>
      {children}
    </th>
  );
};

const Row = ({ coupon }: { coupon: Coupon }) => {
  return (
    <tr>
      <Td>{coupon.code}</Td>
      <Td>{coupon.restaurant.name}</Td>
      <Td>{coupon.redeemCount}</Td>
      <Td>{coupon.issueCount}</Td>
      <Td>
        <div className="flex flex-row gap-4 items-center">
          <Link href={{ pathname: "/coupons/edit/[id]", query: { id: coupon.id } }}>Edit</Link>
          <Link href={{ pathname: "/coupons/issue/[id]", query: { id: coupon.id } }}>Issue</Link>
          <Link href={{ pathname: "/coupons/redeem/[id]", query: { id: coupon.id } }}>Redeem</Link>
          <Dialog queryKey="jobs" url="/admin/coupon/delete/" id={`${coupon.id}`} title={"Coupon"} />
        </div>
      </Td>
    </tr>
  );
};
