import { useRouter } from "next/router";
import Layout from "../../../components/layout.comp";
import { useQuery, useQueryClient } from "@tanstack/react-query";

import { Td, TdDate, Th } from "../../../components/table.comp";
import useCoupon from "../../../hooks/useCoupon.hook";
import { useForm } from "react-hook-form";
import { FancySelect } from "../../../components/form.comp";
import { listUsers } from "../../../core/user.core";
import { issue, listIssue } from "../../../core/issue.core";

export default function RedeemPage() {
  return (
    <Layout title="Issue Coupon">
      <h1>Issue Coupon</h1>
      <Info />
      <IssueList />
      <hr />
      <IssueAction />
    </Layout>
  );
}

const Info = () => {
  const { data: coupon, isFetched } = useCoupon();

  if (!isFetched) return null;

  return (
    <div>
      <h2 className="mb-2 font-bold">Coupon Info</h2>
      <div className="border border-black p-2">
        <div>Code: {coupon.code}</div>
        <div>Restaurant: {coupon.restaurant.name}</div>
      </div>
    </div>
  );
};

const IssueList = () => {
  const { query, isReady } = useRouter();
  const couponId = query.id as string;

  const { data: issueList, isFetched } = useQuery({
    queryKey: ["issueList", couponId],
    queryFn: (context) => listIssue(context.queryKey[1] as string),
    enabled: isReady,
  });

  if (!isFetched) return null;
  if (issueList === undefined) return null;

  return (
    <div>
      <h2 className="font-bold mb-2">Issue History</h2>
      <table>
        <tr>
          <Th>User ID</Th>
          <Th>Date</Th>
        </tr>
        {issueList.map((r) => {
          return (
            <tr key={r.id}>
              <Td>{r.user.id}</Td>
              <TdDate date={r.at} />
            </tr>
          );
        })}
      </table>
    </div>
  );
};

interface RedeemForm {
  userId: string;
}

const IssueAction = () => {
  const methods = useForm<RedeemForm>();
  const { query, isReady } = useRouter();
  const queryClient = useQueryClient();

  const { data: users, isFetched } = useQuery({
    queryKey: ["users"],
    queryFn: listUsers,
  });

  if (!isFetched) return null;
  if (!isReady) return null;

  const couponId = query.id as string;

  async function onSubmit(data: RedeemForm) {
    await issue(couponId, data.userId);
    queryClient.invalidateQueries({ queryKey: ["issueList", couponId] });
  }

  return (
    <div>
      <h1 className="font-bold mb-2">Issue Action</h1>

      <form onSubmit={methods.handleSubmit(onSubmit)}>
        <FancySelect
          register={methods.register}
          fieldName="userId"
          options={users.map((u) => {
            return {
              value: u.id,
              label: `${u.id} (${u.signInProvider})`,
            };
          })}
        />
        <button type="submit" className="border border-black px-2 py-1 mt-2">
          Issue
        </button>
      </form>
    </div>
  );
};
