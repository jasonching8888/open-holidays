import { useEffect } from "react";

import Layout from "../components/layout.comp";
import { redirectIfLogined } from "../core/login.core";
import { useTranslation } from "../core/translation.core";

export default function IndexPage() {
  const { t, msg } = useTranslation();
  useEffect(() => {
    redirectIfLogined();
  }, []);
  return (
    <Layout title={t(msg.index.dashboard)}>
      <h1>{t(msg.index.dashboard)}</h1>
    </Layout>
  );
}
