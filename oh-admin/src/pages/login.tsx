import Head from "next/head";
import { useEffect } from "react";
import { useState } from "react";
import { useForm } from "react-hook-form";

import { LabelAndInput } from "../components/form.comp";
import { performLogin, redirectIfLogined } from "../core/login.core";

interface FormType {
  email: string;
  password: string;
}

export default function LoginPage() {
  const { register, handleSubmit } = useForm<FormType>();
  const [errMsg, setErrMsg] = useState<string>();

  useEffect(() => {
    redirectIfLogined();
  }, []);

  const onSubmit = async (data) => {
    const rst = await performLogin(data);

    if (rst === false) {
      setErrMsg("Failed to Login");
    }
  };

  return (
    <>
      <Head>
        <title>Login</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <div className="flex justify-center items-center h-screen flex-col">
        <h1>Login</h1>
        <div className="border w-full md:w-3/12 m-2 p-2">
          {errMsg && (
            <p id="err" className="text-red-700">
              {errMsg}
            </p>
          )}
          <form onSubmit={handleSubmit(onSubmit)}>
            <LabelAndInput type="email" register={register} fieldName="email" label="Email Address" />
            <LabelAndInput type="password" register={register} fieldName="password" label="Password" />
            {/* 
            <label className="mt-3">
              <p className="mb-2">Email Address:</p>
              <input type="email" className="h-9 border w-full" {...register("email", { required: true })} />
            </label>
            <label className="mt-3">
              <p className="mb-2">Password:</p>
              <input className="h-9 border w-full" type="password" {...register("password", { required: true })} />
            </label> */}
            <input className="mt-3 border border-gray-400 py-0.5 px-3" type="submit" value="Submit" />
          </form>
        </div>
      </div>
    </>
  );
}
