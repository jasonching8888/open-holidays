import { useQuery } from "@tanstack/react-query";
import Layout from "../../components/layout.comp";
import { getRedeemReport } from "../../core/coupon.core";
import { Td, TdDate, Th } from "../../components/table.comp";

export default function ReportPage() {
  return (
    <Layout title="Coupon Redemption Report">
      <h1>Coupon Redemption Report</h1>
      <List />
    </Layout>
  );
}

const List = () => {
  const { data: coupons, isFetched } = useQuery({
    queryKey: ["redeemReport"],
    queryFn: getRedeemReport,
  });

  if (!isFetched) return null;

  return (
    <table>
      <tr>
        <Th>Coupon Code</Th>
        <Th>Restaurant</Th>
        <Th>Total Redeem</Th>
        <Th>List</Th>
      </tr>
      {coupons.map((c) => (
        <ReportSection key={c.id} coupon={c} />
      ))}
    </table>
  );
};

const ReportSection = ({ coupon }: { coupon: ReportCoupon }) => {
  return (
    <tr>
      <Td>
        <span className="font-bold">{coupon.code}</span>
      </Td>
      <Td>
        <span className="text-green-500">{coupon.restaurant}</span>
      </Td>
      <Td>{coupon.redeem.length}</Td>
      <Td>
        <table className="mt-2">
          <tr>
            <Th>Date</Th>
            <Th>User ID</Th>
          </tr>
          {coupon.redeem.map((r) => {
            return (
              <tr key={r.id}>
                <TdDate date={r.at} />
                <Td>{r.user.id}</Td>
              </tr>
            );
          })}
        </table>
      </Td>
    </tr>
  );
};
