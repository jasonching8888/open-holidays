import { useForm } from "react-hook-form";
import RestaurantEditor from "../../components/restaurantEditor.comp";
import { EditingMode } from "../../components/form.comp";
import Layout from "../../components/layout.comp";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { createRestaurant } from "../../core/restaurant.core";
import { useRouter } from "next/router";

export default function CreateCouponPage() {
  const methods = useForm<RestaurantForm>();
  const {push} = useRouter();
  const queryClient = useQueryClient();

  const mutation = useMutation({
    mutationFn: async (data: RestaurantForm) => createRestaurant(data),
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ["restaurants"] });
      push("/restaurants");
    },
  });
  
  return <Layout title="Add Restaurant">
      <RestaurantEditor
        editingMode={EditingMode.create}
        methods={methods}
        onSubmit={(data) => mutation.mutateAsync(data)}
        jobMutation={mutation}
      />
    </Layout>
}