import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { useForm } from "react-hook-form";

import { EditingMode } from "../../../components/form.comp";
import Layout from "../../../components/layout.comp";
import { getRestaurant, editRestaurant } from "../../../core/restaurant.core";
import RestaurantEditor from "../../../components/restaurantEditor.comp";

export default function RestaurantEditPage() {
  const methods = useForm<RestaurantForm>();
  const { push, query, isReady } = useRouter();
  const queryClient = useQueryClient();

  const { data: restaurantForm } = useQuery({
    queryKey: ["restaurants", query.id],
    queryFn: async (context) => {
      const r = await getRestaurant(context.queryKey[1] as string);
      return convertData(r);
    },
    enabled: isReady,
  });

  function convertData(restaurant: Restaurant): RestaurantForm {
    return {
      name: restaurant.name,
    };
  }

  useEffect(() => {
    if (restaurantForm) {
      methods.reset(restaurantForm);
    }

    // eslint-disable-next-line
  }, [restaurantForm]);

  const jobMutation = useMutation({
    mutationFn: async (data: RestaurantForm) => editRestaurant(query.id as string, data),
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ["restaurants"] });
      push("/restaurants");
    },
  });

  return (
    <Layout title="Edit Restaurant">
      <RestaurantEditor
        editingMode={EditingMode.edit}
        methods={methods}
        onSubmit={(data) => jobMutation.mutateAsync(data)}
        jobMutation={jobMutation}
      />
    </Layout>
  );
}
