import { useQuery } from "@tanstack/react-query";
import Link from "next/link";
import { UseFormReturn, useForm, useWatch } from "react-hook-form";

import Dialog from "../../components/dialog.comp";
import Layout from "../../components/layout.comp";
import Spinner from "../../components/spinner.comp";
import { Td } from "../../components/table.comp";
import { table, tableBody, tableHead, tableHeadRow } from "../../styles/table.css";
import { FilterForm } from "../../types/filter";
import { getRestaurantList } from "../../core/restaurant.core";

export default function RestaurantListPage() {
  const methods = useForm<FilterForm>({
    defaultValues: {
      paging: { size: 50, page: 1 },
      sort: "asc",
      status: "",
      clientId: "",
      range: "",
      workingPartnerIds: "",
      referralPartnerIds: "",
      to: null,
      from: null,
      jobNatureId: "",
    },
  });
  const filterValues = useWatch({ control: methods.control });

  const {
    data: restaurants,
    isLoading,
    isError,
    isFetching,
  } = useQuery({
    queryKey: ["restaurants"],
    queryFn: () => getRestaurantList(),
  });

  if (isError) {
    return <h1>Fetching Error</h1>;
  }

  if (isLoading) {
    return <Spinner />;
  }

  return (
    <Layout title="Shops">
      <div className="flex flex-col">
        <div className="flex flex-row justify-between justify-items-center  ">
          <h1>Shops</h1>
          <ul className="flex flex-col ">
            <li className="inline-flex items-center cursor-pointer gap-x-2.5 py-3 px-4 text-sm font-medium bg-white border text-gray-800 -mt-px first:rounded-t-lg first:mt-0 last:rounded-b-lg sm:-ml-px sm:mt-0 sm:first:rounded-tr-none sm:first:rounded-bl-lg sm:last:rounded-bl-none sm:last:rounded-tr-lg dark:bg-gray-800 dark:border-gray-700 dark:text-white ">
              <Link className="hover:text-blue-600 " href="/restaurants/create">
                Add Shop
              </Link>
            </li>
          </ul>
        </div>
        <div className="flex flex-col gap-4">
          <RestaurantList isFetching={isFetching} restaurants={restaurants} methods={methods} />
        </div>
      </div>
    </Layout>
  );
}

const RestaurantList = ({
  restaurants,
  isFetching,
  methods,
}: {
  restaurants: Restaurant[];
  isFetching: boolean;
  methods: UseFormReturn<FilterForm, any>;
}) => {
  if (isFetching)
    return (
      <div className="overflow-x-auto w-full md:w-4/05">
        <Spinner />
      </div>
    );

  if (restaurants.length === 0)
    return <div className="overflow-x-auto w-full md:w-4/05 flex justify-center text-lg">No Records Found</div>;

  return (
    <>
      <div className="overflow-x-auto w-full md:w-4/05">
        <table className={table}>
          <thead className={tableHead}>
            <tr>
              <Th>Name</Th>
              {/* <Th>Restaurant</Th> */}
              <Th> </Th>
            </tr>
          </thead>
          <tbody className={tableBody}>
            {restaurants.map((restaurant) => (
              <Row key={restaurant.id} restaurant={restaurant} />
            ))}
          </tbody>
        </table>
      </div>

      {/* <PageNavForm pageField="paging.page" sizeField="paging.size" total={jobRes.total} methods={methods} /> */}
    </>
  );
};

const Th = ({ children }: { children: React.ReactNode }) => {
  return (
    <th scope="col" className={tableHeadRow}>
      {children}
    </th>
  );
};

const Row = ({ restaurant }: { restaurant: Restaurant }) => {
  return (
    <tr>
      <Td>{restaurant.name}</Td>
      {/* <Td>{coupon.restaurant.name}</Td> */}
      <Td>
        <div className="flex flex-row gap-4 items-center">
          <Link href={{ pathname: "/restaurants/edit/[id]", query: { id: restaurant.id } }}>Edit</Link>
          <Dialog queryKey="restaurants" url="/admin/restaurant/delete/" id={`${restaurant.id}`} title={"Restaurant"} />
        </div>
      </Td>
    </tr>
  );
};
