import Link from "next/link";

import Layout from "../components/layout.comp";
import ProfileLocale from "../components/profileLocale";
import ProfileMe from "../components/profileMe";
import { useTranslation } from "../core/translation.core";

export default function SettingPage() {
  const { t, msg } = useTranslation();

  return (
    <Layout title={t(msg.setting.setting)}>
      <h1>{t(msg.setting.setting)}</h1>
      <ProfileMe />
      <ProfileLocale />
    </Layout>
  );
}
