import { useRouter } from "next/router";

import Error from "../../components/error.comp";
import Layout from "../../components/layout.comp";
import AdminUserEditor from "../../components/userEditor.comp";
import userCore from "../../core/user.core";
// import { AdminUserFormData } from "../../types/user";

export default function CreateAdminUserPage() {
  const { push } = useRouter();

  async function create(data: AdminUserFormData) {
    await userCore.addUser(data);
    push("/users");
  }

  return (
    <Layout title="Create AdminUser">
      <div>
        <h1>Create AdminUser</h1>
        <Error />
        <AdminUserEditor onSubmit={create} />
      </div>
    </Layout>
  );
}
