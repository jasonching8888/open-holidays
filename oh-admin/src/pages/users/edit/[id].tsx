import { useQuery } from "@tanstack/react-query";
import { useRouter } from "next/router";

import Layout from "../../../components/layout.comp";
import AdminUserEditor from "../../../components/userEditor.comp";
import { useTranslation } from "../../../core/translation.core";
import userCore from "../../../core/user.core";
// import { AdminUserFormData } from "../../../types/user";

export default function UserEditPage() {
  const { query, isReady, push } = useRouter();
  const { t, msg } = useTranslation();

  const { data: user, isFetched } = useQuery({
    queryKey: ["user", query.id],
    queryFn: (context) => userCore.getUser(context.queryKey[1] as string),
    enabled: isReady,
  });

  async function updateUser(data: AdminUserFormData) {
    await userCore.updateUser(data);
    push("/users");
  }

  return (
    <Layout title="Edit User">
      <h1>{t(msg.user.edit)}</h1>
      {isFetched && <AdminUserEditor user={user} onSubmit={updateUser} />}
    </Layout>
  );
}
