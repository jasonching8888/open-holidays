import { useQuery } from "@tanstack/react-query";
import { useState } from "react";

import Error from "../../components/error.comp";
import Layout from "../../components/layout.comp";
import Spinner from "../../components/spinner.comp";
import userCore, { listUsers } from "../../core/user.core";
import { table, tableBody, tableDefinition, tableHead, tableHeadRow } from "../../styles/table.css";
// import { User } from "../../types/user";

export default function UserListingPage() {
  return (
    <Layout title="User List">
      <div>
        <h1>User List</h1>
        <Error />
        {/* <Link href="/user/create">Add User</Link> */}
        {/* <div className="flex justify-end mt-3">
          <ExportUser excelSheets={["users"]} name="user" columnsToExclude={[4, 5]} />
        </div> */}
        <List />
      </div>
    </Layout>
  );
}

const List = () => {
  const query = useQuery({
    queryKey: ["users"],
    queryFn: listUsers,
  });

  if (query.isLoading) return <Spinner />;
  if (query.isError) return <h1>Error...</h1>;

  return (
    <div className="overflow-x-auto">
      <table id="adminUser-list" className={table}>
        <thead className={tableHead}>
          <tr>
            <th className={tableHeadRow}>ID</th>
            <th className={tableHeadRow}>UID</th>
            <th className={tableHeadRow}>Provider</th>
          </tr>
        </thead>
        <tbody className={tableBody}>
          {query.data.map((p) => (
            <UserRow user={p} key={p.id} />
          ))}
        </tbody>
      </table>
    </div>
  );
};

const UserRow = ({ user }: { user: User }) => {
  const className = "border border-slate-700 px-6 py-4 whitespace-nowrap text-sm text-gray-800 dark:text-gray-200";
  return (
    <tr attr-name={user.id}>
      <td className={tableDefinition}>{user.id}</td>
      <td className={tableDefinition}>{user.uid}</td>
      <td className={tableDefinition}>{user.signInProvider}</td>
    </tr>
  );
};

const ResendInvitation = ({ adminUser }: { adminUser: User }) => {
  const [status, setStatus] = useState<"none" | "sending" | "sent">("none");

  // if (adminUser.status !== "pending") return null;

  async function resend() {
    setStatus("sending");
    await userCore.resendInvitation(adminUser.id);
    setStatus("sent");
  }

  const SendStatus = () => {
    switch (status) {
      case "none":
        return null;
      case "sending":
        return <span>Sending...</span>;
      case "sent":
        return <span className="text-green-400">Sent!</span>;
    }
  };

  return (
    <>
      <button className="underline px-2" onClick={resend} disabled={status === "sending"}>
        Resend Invitation
      </button>

      <SendStatus />
    </>
  );
};
