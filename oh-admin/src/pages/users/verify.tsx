import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";

import Layout from "../../components/layout.comp";
import userCore, { VerifyStatus } from "../../core/user.core";

enum Status {
  verifying,
  pending,
  statusFailed,
  verificationFailed,
  success,
}

interface FormType {
  password: string;
}

export default function VerifyUserPage() {
  const { query, isReady } = useRouter();
  const [status, setStatus] = useState<Status>(Status.verifying);

  useEffect(() => {
    if (!isReady) return;

    verifyStatus();

    // eslint-disable-next-line
  }, [query, isReady]);

  async function verifyStatus() {
    const status = await userCore.verifyUserStatus(query.userId as string, query.emailVerificationCode as string);

    if (status !== VerifyStatus.pending) {
      setStatus(Status.pending);
    } else {
      setStatus(Status.statusFailed);
    }
  }

  const Verifying = () => {
    if (status !== Status.verifying) return null;
    return <div>Is Verifying...</div>;
  };

  const VerifyForm = () => {
    const { register, handleSubmit } = useForm<FormType>();

    if (status !== Status.pending) return null;

    async function onSubmit(data: FormType) {
      await userCore.verifyUser(query.userId as string, query.emailVerificationCode as string, data.password);
      setStatus(Status.success);
    }

    return (
      <form onSubmit={handleSubmit(onSubmit)} id="form-verifyUser">
        <label>
          Password
          <input type="password" placeholder="Password" className="h-9 border w-full" {...register("password")} />
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  };

  const Success = () => {
    if (status !== Status.success) return null;

    return (
      <div id="success">
        <h1 className="text-green-400">Your Account is Verified Successfully!</h1>
        <Link id="login" className="underline" href="/login">
          Login Here
        </Link>
      </div>
    );
  };

  return (
    <Layout title="Verifying User">
      <h1>Verifying</h1>
      <Verifying />
      <VerifyForm />
      <Success />
    </Layout>
  );
}
