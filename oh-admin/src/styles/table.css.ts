const table =
  "my-3 border-collapse border border-slate-500 table-auto min-w-full divide-y divide-gray-200  dark:divide-gray-700";
const tableHead = "bg-gray-50 dark:bg-gray-700";
const tableDefinition =
  "border border-slate-700 px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-800 dark:text-gray-200";
const tableHeadRow = "border border-slate-700 px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase";
const tableBody = "divide-y divide-gray-200 dark:divide-gray-700";

export { table, tableHead, tableDefinition, tableHeadRow, tableBody };
