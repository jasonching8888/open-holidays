type CouponStatus = "enabled" | "disabled";

interface Coupon {
  id: string;
  code: string;
  discount: number;
  status: CouponStatus;
  usageLimit: number;
  restaurant: {
    id: string;
    name: string;
  };
  redeemCount: number;
  issueCount: number;
}

interface CouponForm {
  id: string;
  code: string;
  discount: number;
  usageLimit: number;
  status: CouponStatus;
  restaurantId: string;
}

interface Redeem {
  id: string;
  at: Date;
  user: User;
}

interface ReportRedeem {
  id: string;
  at: Date;
  user: User;
}

interface ReportCoupon {
  id: string;
  code: string;
  restaurant: string;
  redeem: ReportRedeem[];
}
