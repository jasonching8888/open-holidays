export interface Customer {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  emailVerified: boolean;
  provider: string;
  createdAt: Date;
}
