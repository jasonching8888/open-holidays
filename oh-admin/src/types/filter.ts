export interface Filter {
  paging: {
    size: number;
    page: number;
  };
  sort?: "asc" | "desc";
  filter?: filterFIelds;
  range?: { fieldName: string; from: Date; to: Date };
}
export interface filterFIelds {
  clientId: string;
  referralPartnerIds: string;
  workingPartnerIds: string;
  jobNatureId: string;
  status: string;
}

export interface FilterForm {
  paging: {
    size: number;
    page: number;
  };
  clientId: string;
  referralPartnerIds: string;
  workingPartnerIds: string;
  jobNatureId: string;
  status: string;
  sort: "asc" | "desc";
  range: string;
  from: Date;
  to: Date;
}
