export interface LocaleText {
  en: string;
  zhHant: string;
  zhHans: string;
}
