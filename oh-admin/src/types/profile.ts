// import { AdminUserRole } from "./user";

export interface Me {
  id: string;
  role: AdminUserRole;
  email: string;
}
