interface Restaurant {
  id: string;
  name: string;
}

interface RestaurantForm {
  name: string;
}
