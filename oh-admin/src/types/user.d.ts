type AdminUserStatus = "pending" | "created" | "deactivated";
type AdminUserRole = "admin" | "regular";

interface User {
  id: string;
  uid: string;
  signInProvider: string;
}

interface AdminUserFormData {
  id: string;
  email: string;
  name: string;
  status: AdminUserStatus;
  role: AdminUserRole;
}
