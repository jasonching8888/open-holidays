# Open Holidays Club

## Core Functions

| Core Functions | Issued By     | Received By                | Operation                                                                 |
| -------------- | ------------- | -------------------------- | ------------------------------------------------------------------------- |
| Issue Coupon   | Open Holidays | Customers                  | Issue coupon in backend console.                                          |
| Redeem Coupon  | Shop Staff    |                            | Scan QR Code on the app.                                                  |
| Reporting      |               | Shop Manager Open Holidays | See / Download the report in backend console.                             |
| Notification   |               | Customers                  | Once coupon is issued or redeemed, notification will be sent to customer. |

## Coupon Limitation

There are 2 limitations: `global limitation` (gLimit) and `individual limiation` (iLimit).

Global limiation is the max number of coupon that can be issued.
Individual limiation is the max number of redemption each individual can make.

For example,

- gLimit=undefined & iLimit=1: means each there is max issue limit, and each person can only redeem once.
- gLimit=100 & iLimit=1: means the coupon is limited to 100. Each people can only redeem

## Coupon Issuing Logic

### Secret Link

This is the most flexible approach we can think of right now.

The link will be something like this:

`https://club.openholidays.hk/collect?campaign=EWRKsk2AKS`

The term `secret` means that there is no way you can guess this link right. So only the receiver of this link can open it and collect the coupon.

Users who click on this link will be routed to the app and the coupon will be generated and issued to the user.
If app is not installed, the page will guide them to install the app first.

Possible scenarios:

1. Use EDM or make a Facebook post to broadcast this link to subscribers. We can set limit here. Only the first 100 users can redeem.
1. Do some on site promotional event. Print this secret link as a QR code. Put it somewhere in the shopping mall. Let participants find them.

## System Details

### Deliverables

- Mobile App (Android, iOS, Flutter)
- Backend Console (Web Based, ReactJS)
- Server (Node.JS)
- Database (MongoDB)

### Hosting

- AWS EC2 `t2.micro`

## Logic Flow

### User Journey

```mermaid
journey
    Receive Coupon: 4
    Go to the shop: 5
    Redeemp Coupon: 5
```

### Coupon Issue and Redemption

```mermaid
sequenceDiagram
    actor OH as Open Holidays
    participant App
    actor Alice
    actor SS as Shop Staff
    actor SM as Shop Manager

    OH->>App: Issue Coupon
    App->>Alice: Coupon Collected<br />(App Notification)
    Alice->>SS: Show QR Code on Mobile App
    Alice->>Alice: Go to the shop
    SS->>Alice: Scan QR Code
    App->>App: Coupon Redeemed
    App-->>SM: Reporting
    App-->>OH: Reporting
    Alice->>App: See Redeemption<br />Records
```

## Technical

### Coupon Model

The so called unique coupon is actually not unique in database1. This is how it works:

```javascript
const coupon = {
  _id: ObjectId("62fef0378852262d1206e03e"),
  name: "Coupon 1",
  g_limt: 100,
  i_limit: 1,
  redeemed_users: [ObjectId("62fef0378852262d1206e03e")],
};
```

```javascript
const user = {
  _id: ObjectId("62fef0408852262d1206e03f"),
  name: "xxx",
  // whatever else properties...
  coupons: [ObjectId("62fef0378852262d1206e03e")],
};
```

In this case, the QR code of this user's coupon will be:

`ohc:62fef0408852262d1206e03f|62fef0408852262d1206e03f`

_ojhc stands for open holidays coupon_

It contains both user_id and coupon_id for us to do all the lookup and necessary validation.
