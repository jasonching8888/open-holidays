export default interface Config {
  env: string;
  database: string;
  adminFrontUrl: string;
  smtp: {
    host: string;
    port: number;
    auth: {
      user: string;
      pass: string;
    };
  };
  smtpFrom: string;
  stripe: {
    pk: string;
    secret: string;
  };
  url: string;
}
