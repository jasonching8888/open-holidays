import Config from "./config";
import devConfig from "./development.config";
// import prodConfig from "./production.config";
import testConfig from "./test.config";

export default function getConfigByEnv(): Config {
  switch (process.env.NODE_ENV) {
    case "development":
      return devConfig;
    case "test":
      return testConfig;
    default:
      break;
  }
}
