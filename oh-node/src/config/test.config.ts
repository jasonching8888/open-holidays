import Config from "./config";

const config: Config = {
  env: "test",
  database: "mongodb://127.0.0.1:27017/openholiday-test",
  adminFrontUrl: "http://localhost:3009",
  smtp: {
    host: "smtp.ethereal.email",
    port: 587,
    auth: {
      user: "fidel.orn@ethereal.email",
      pass: "ejCD37Fz1FSprHyM9x",
    },
  },
  smtpFrom: '"HER Admin 👻" <admin@shoppick.com>"',

  stripe: {
    pk: "pk_test_51Hdv52EpNcfo5JDWE6rs8dFSHOPOWevx0xgNsq9j2MAYWmnQvy2yRpGyqWjFBqnsNdhoVsnZgQZUNr37uOAHyJNH00gatXSHp5",
    secret:
      "sk_test_51Hdv52EpNcfo5JDW3W0zhMJ33xpDP1yNpJO0yUcaaaZfLiLUJYHmd4md0XCC23Izkfs9oXLj0jhPGNPQQutxfWmb002kNb3nkv",
  },
  url: "http://localhost:5002/public/webhook/endpoint",
};

export default config;
