import crypto from "crypto";

import nodemailer from "nodemailer";

import AdminUserModel, { AdminUserDocLean } from "../models/adminUser.model";
import MailModel, { MailType } from "../models/mail.model";
import adminUserUtil from "../utils/adminUser.utils";
import Config from "../utils/config";

async function addUserCreateNew(email: string, role: string, name: string) {
  const uuid = crypto.randomUUID();

  const createdAdminUser = await AdminUserModel.create({
    email,
    name,
    role,
    emailVerificationCode: uuid,
    status: "pending",
    internalPassword: adminUserUtil.randomInternalPassword(),
  });

  await emailInvitation(createdAdminUser);

  return createdAdminUser;
}

async function updateUser(
  id: string,
  email: string,
  role: string,
  name: string,
  emailVerificationCode: string,
  status: string,
  internalPassword: string
) {
  const modifiedUser = await AdminUserModel.updateOne({
    id,
    email,
    name,
    role,
    emailVerificationCode,
    status,
    internalPassword,
  });

  // await emailInvitation(modifiedUser);

  return modifiedUser;
}

async function resendInvitation(email: string) {
  const adminUser = await AdminUserModel.findOne({
    email,
  }).lean();

  await emailInvitation(adminUser);
}

async function emailInvitation(user: AdminUserDocLean) {
  const url = `${Config.adminFrontUrl}/user/verify?emailVerificationCode=${user.emailVerificationCode}&userId=${
    user._id.toString() as string
  }`;
  const msgHtml = `Your registration link is:<br /><a href="${url}">${url}</a>`;

  const transporter = nodemailer.createTransport(Config.smtp);
  const mailOptions = {
    from: Config.smtpFrom,
    to: user.email,
    subject: "You Are Invitied To Join CAD Design ✔",
    html: msgHtml,
  };

  const mail = await MailModel.create({
    adminUserId: user._id,
    message: mailOptions,
    mailType: MailType.adminUserRegistration,
  });

  // send email
  transporter
    .sendMail(mailOptions)
    .then(async (info) => {
      await MailModel.findOneAndUpdate(
        { _id: mail._id },
        {
          $set: {
            sentInfo: info,
          },
        }
      );
    })
    .catch(async (e) => {
      console.error(e);
      await MailModel.findOneAndUpdate(
        { _id: mail._id },
        {
          $set: {
            sentInfo: e,
          },
        }
      );
    });
}

export default {
  addUserCreateNew,
  resendInvitation,
  updateUser,
  emailInvitation,
};
