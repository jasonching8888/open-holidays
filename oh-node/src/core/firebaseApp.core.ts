import admin from "firebase-admin";
import { App, initializeApp } from "firebase-admin/app";

let app: App;

function init() {
  const gearAtticCredential = getFirebaseKey("open-holiday");

  app = initializeApp({
    credential: admin.credential.cert(gearAtticCredential),
  });

  console.log("firebase-app init done.");
}

function getFirebaseKey(filename: string) {
  return require(`../../firebase_keys/${filename}.json`);
}

export default {
  init,
  app,
};
