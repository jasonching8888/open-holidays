// import crypto from "crypto";

// import Debug from "debug";
// import mongoose from "mongoose";

// import { MailType } from "../models/mail.model";
// import { MailTemplateType } from "../models/mailTemplate.model";
// import { MediaDocLean } from "../models/media.model";
// import { ShopDocLean } from "../models/shop.model";
// import UserModel, { UserDoc, UserDocLean } from "../models/user.model";
// import mailCore from "./mail.core";

// // eslint-disable-next-line
// const debug = Debug("project:frontUser.core");

// async function requestResetPwd(shop: ShopDocLean, email: string) {
//   const frontUser = await setResetPwdToUser({
//     shop_id: shop._id,
//     email,
//   });

//   if (frontUser === null || frontUser === undefined) {
//     return {
//       rst: false,
//       message: "noUser",
//     };
//   }

//   if (frontUser === null || frontUser === undefined) {
//     return {
//       rst: false,
//       message: "noUser",
//     };
//   }

//   const rst = await sendResetPwdEmail(shop, frontUser);
//   return rst;
// }

// async function setResetPwdToUser(filter?: mongoose.FilterQuery<UserDoc>) {
//   const code = crypto.randomUUID();
//   return await UserModel.findOneAndUpdate(
//     filter,
//     {
//       $set: {
//         resetPwCode: code,
//         resetPwCodeDate: new Date(),
//       },
//     },
//     {
//       new: true,
//     }
//   ).lean();
// }

// function getResetPwdUrl(shop: ShopDocLean, frontUser: UserDocLean) {
//   return `${shop.root_url}auth/resetPwd?code=${frontUser.resetPwCode}&userId=${frontUser._id.toString() as string}`;
// }

// async function sendResetPwdEmail(shop: ShopDocLean, frontUser: UserDocLean) {
//   const url = getResetPwdUrl(shop, frontUser);
//   const shopLogoUrl = (shop.logo_media_id as MediaDocLean)?.thumbnailUrl;

//   const mailOptions = {
//     templateType: "passwordReset" as MailTemplateType,
//     mailType: "frontResetPwd" as MailType,
//     shopId: shop._id,
//     data: { userName: frontUser.first_name, passwordResetUrl: url, shopLogoUrl },
//   };

//   const rst = await mailCore.sendMailWithTemplate({
//     ...mailOptions,
//     receiver: { email: frontUser.email },
//   });

//   return rst;
// }

// export default {
//   getResetPwdUrl,
//   setResetPwdToUser,
//   requestResetPwd,
// };
