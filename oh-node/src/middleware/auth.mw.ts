// import Debug from "debug";
import express from "express";
import { DecodedIdToken, getAuth } from "firebase-admin/auth";

import firebaseApp from "../core/firebaseApp.core";
import UserModel, { UserDocLean, UserPoJo, UserType } from "../models/user.model";

const env = process.env.NODE_ENV || "development";

// const debug = Debug("project:validaetRequest");

export default async function (req: express.Request, res: express.Response, next: express.NextFunction) {
  // debug("validateRequest");
  try {
    const token = req.headers["x-access-token"] as string;

    try {
      const decodedIdToken = await verifyToken(token);
      req.currentUser = await findOrCreateUser(decodedIdToken);

      // debug("currentUser", req.currentUser);
    } catch (e) {
      console.error("validateUser error", e);
      return res.status(500).json(e);
    }

    next();
  } catch (e) {
    console.error("validateRequestApp error", e);
    res.status(500).json("something went wrong at server");
  }
}

async function verifyToken(token: string): Promise<DecodedIdToken> {
  if (env === "test") {
    return JSON.parse(token) as DecodedIdToken;
  }

  return await getAuth(firebaseApp.app).verifyIdToken(token);
}

async function findOrCreateUser(decodedIdtoken: DecodedIdToken): Promise<UserDocLean> {
  const user = await UserModel.findOne({
    uid: decodedIdtoken.uid,
  }).lean();

  if (user === null) {
    // create user
    const userPoJo: UserPoJo = {
      uid: decodedIdtoken.uid,
      sign_in_provider: decodedIdtoken.firebase.sign_in_provider,
      user_type: UserType.none,
      device_tokens: [],
      createdOn: new Date(),
      visibility: "public",
    };

    return await UserModel.create(userPoJo);
  }

  return user;
}
