import express from "express";

export default async function isAdmin(req: express.Request, res: express.Response, next: express.NextFunction) {
  if (req.adminUser.role === "admin") {
    return next();
  }

  res.status(401).json({ message: "Only admin is allowed for this operation." });
}
