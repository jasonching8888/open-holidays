import mongoose from "mongoose";

export type AdminUserRole = "admin" | "regular";
export type AdminUserStatus = "pending" | "created" | "deactivated";

export interface AdminUserPoJo {
  email: string;
  name: string;
  role: AdminUserRole;
  passwordHash: string;
  internalPassword: string;
  status: AdminUserStatus;
  emailVerificationCode: string;
  createdAt: Date;
}

export interface AdminUserDoc extends AdminUserPoJo, mongoose.Document {}
export type AdminUserModel = mongoose.Model<AdminUserDoc>;
export type AdminUserDocLean = mongoose.LeanDocument<AdminUserDoc>;

const schema = new mongoose.Schema<AdminUserPoJo>({
  email: { type: String, required: true, unique: true },
  name: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    enum: ["admin", "regular"],
    required: true,
  },
  passwordHash: {
    type: String,
  },
  internalPassword: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    enum: ["pending", "created", "deactivated"],
    default: "pending",
  },
  emailVerificationCode: {
    type: String,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

const model = mongoose.model<AdminUserDoc, AdminUserModel>("AdminUser", schema);

export default model;
