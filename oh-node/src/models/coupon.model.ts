import mongoose from "mongoose";

import { CouponRedeemDocLean, CouponRedeemSchema } from "./couponRedeem.model";
import { RestaurantDocLean } from "./restaurant.model";

export type CouponStatus = "enabled" | "disabled";
export const CouponStatusArr = ["enabled", "disabled"];

export interface CouponPoJo {
  code: string;
  restaurantId: mongoose.Types.ObjectId | RestaurantDocLean;
  discount: number;
  usageLimit: number;
  status: CouponStatus;
  redeem: CouponRedeemDocLean[];
  issue: CouponRedeemDocLean[];
  createdAt?: Date;
}

export interface CouponDoc extends CouponPoJo, mongoose.Document {}
export type CouponModel = mongoose.Model<CouponDoc>;
export type CouponDocLean = mongoose.LeanDocument<CouponDoc>;

const schema = new mongoose.Schema<CouponPoJo>({
  code: {
    type: String,
    required: true,
  },
  restaurantId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Restaurant",
    required: true,
  },
  discount: {
    type: Number,
    required: true,
  },
  usageLimit: {
    type: Number,
    required: true,
  },
  redeem: {
    type: [CouponRedeemSchema],
    default: [],
    required: true,
  },
  issue: {
    type: [CouponRedeemSchema],
    default: [],
    required: true,
  },
  status: {
    type: String,
    enum: CouponStatusArr,
    default: "enabled",
    required: true,
  },
});

const model = mongoose.model<CouponDoc, CouponModel>("Coupon", schema);

export default model;
