import mongoose from "mongoose";

import { UserDocLean } from "./user.model";

export interface CouponRedeemPoJo {
  userId: mongoose.Types.ObjectId | UserDocLean;
  at: Date;
}

export interface CouponRedeemDoc extends CouponRedeemPoJo, mongoose.Document {}
export type CouponRedeemModel = mongoose.Model<CouponRedeemDoc>;
export type CouponRedeemDocLean = mongoose.LeanDocument<CouponRedeemDoc>;

export const CouponRedeemSchema = new mongoose.Schema<CouponRedeemPoJo>({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  at: {
    type: Date,
    default: Date.now,
    required: true,
  },
});
