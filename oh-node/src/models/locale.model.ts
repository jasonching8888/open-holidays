import mongoose from "mongoose";

export type LocaleOptionType = "en" | "zh-Hant";
export const LocaleOptionTypeArr = ["en", "zhHant"];

export enum LocaleOption {
  en = "en",
  zhHant = "zh-Hant",
}

export interface LocaleTextPoJo {
  en: string;
  zhHant: string;
}

export interface LocaleTextDoc extends LocaleTextPoJo, mongoose.Document {}
export type LocaleTextDocLean = mongoose.LeanDocument<LocaleTextDoc>;

export default new mongoose.Schema<LocaleTextPoJo>({
  en: { type: String },
  zhHant: { type: String },
});
