import mongoose from "mongoose";

import { AdminUserDocLean } from "./adminUser.model";

export enum MailType {
  emailVerification = "Email Verification",
  adminUserRegistration = "Admin User Registration",
}

export interface MailPoJo {
  adminUserId: mongoose.Types.ObjectId | AdminUserDocLean;
  message: any;
  mailType: MailType;
  sentInfo: any;
  createdAt: Date;
}

export interface MailDoc extends MailPoJo, mongoose.Document {}
export type MailModel = mongoose.Model<MailDoc>;
export type MailDocLean = mongoose.LeanDocument<MailDoc>;

const schema = new mongoose.Schema<MailPoJo>({
  adminUserId: {
    type: mongoose.Types.ObjectId,
    ref: "AdminUser",
  },
  message: {
    type: mongoose.Schema.Types.Mixed,
    required: true,
  },
  mailType: {
    type: String,
    enum: Object.values(MailType),
    required: true,
  },
  sentInfo: {
    type: mongoose.Schema.Types.Mixed,
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
});

export default mongoose.model<MailDoc, MailModel>("Mail", schema);
