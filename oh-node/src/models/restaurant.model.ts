import mongoose from "mongoose";

export interface RestaurantPoJo {
  name: string;

  createdAt?: Date;
}

export interface RestaurantDoc extends RestaurantPoJo, mongoose.Document {}
export type RestaurantModel = mongoose.Model<RestaurantDoc>;
export type RestaurantDocLean = mongoose.LeanDocument<RestaurantDoc>;

const schema = new mongoose.Schema<RestaurantPoJo>({
  name: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
});

const model = mongoose.model<RestaurantDoc, RestaurantModel>("Restaurant", schema);

export default model;
