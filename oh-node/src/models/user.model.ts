import mongoose from "mongoose";

export enum UserType {
  none = "none",
  employer = "employer",
  helper = "helper",
  admin = "admin",
}

export type Visibility = "public" | "hidden";
export type UserStatus = "unverified" | "hide" | "public";
export const UserStatusArr = ["unverified", "hide", "public"];

export interface DeviceToken {
  token: string;
  locale: string;
  lastUpdated: Date;
}

export interface UserPoJo {
  sign_in_provider: string;
  email?: string;
  uid: string;
  helper_profile?: any;
  employer_profile?: any;
  user_type: UserType;
  avatar_filename?: string;
  device_tokens: DeviceToken[];
  visibility: Visibility;
  createdOn: Date;

  smsOtp?: string;
  smsOtpSentAt?: Date;
  telVerified?: boolean;
  userStatus?: UserStatus;
}

export interface UserDoc extends UserPoJo, mongoose.Document {}

export type UserModel = mongoose.Model<UserDoc>;

export type UserDocLean = mongoose.LeanDocument<UserDoc>;

const schema = new mongoose.Schema({
  sign_in_provider: {
    type: String,
    required: true,
  },
  email: String,
  uid: {
    type: String,
    required: true,
  },
  helper_profile: {
    type: mongoose.Schema.Types.Mixed,
  },
  employer_profile: {
    type: mongoose.Schema.Types.Mixed,
  },
  user_type: {
    type: String,
    enum: ["none", "employer", "helper"],
    default: "none",
  },
  avatar_filename: {
    type: String,
  },
  device_tokens: [
    {
      token: { type: String },
      locale: { type: String },
      lastUpdated: { type: Date, default: Date.now },
    },
  ],
  visibility: {
    type: String,
    enum: ["public", "hidden"],
    default: "public",
  },
  smsOtp: {
    type: String,
  },
  smsOtpSentAt: {
    type: Date,
  },
  telVerified: {
    type: Boolean,
    default: false,
  },
  userStatus: {
    type: String,
    enum: UserStatusArr,
    default: "unverified",
  },
  createdOn: { type: Date, default: Date.now },
});

export default mongoose.model<UserDoc, UserModel>("User", schema);
