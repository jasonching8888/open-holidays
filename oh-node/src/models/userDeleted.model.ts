import mongoose from "mongoose";

export type UserRole = "user" | "admin" | "operator";
export const UserRoles = ["user", "admin", "operator"];

export interface UserDeletedPoJo {
  mobileNumber: string;
  mobileNumberVerified: boolean;
  mobileNumberVerifiedAt: Date;

  smsOtp: string;
  smsOtpCreatedAt: Date;

  passwordHash: string;
  internalPassword: string; // for generating the login token
  resetCodeVerified: boolean;
  resetPwCode: string;
  resetPwCodeDate: Date;
  role: UserRole;

  deleted: boolean;

  // vehicleIds: mongoose.Types.ObjectId[] | VehicleDocLean[];

  createdAt?: Date;
}

export interface UserDeletedDoc extends UserDeletedPoJo, mongoose.Document {}
export type UserDeletedModel = mongoose.Model<UserDeletedDoc>;
export type UserDeletedDocLean = mongoose.LeanDocument<UserDeletedDoc>;

// TODO: Ask hugo, why no OTP for sign up?  How to verify phone number?
const schema = new mongoose.Schema<UserDeletedPoJo>({
  mobileNumber: {
    type: String,
    required: true,
    unique: true,
  },
  mobileNumberVerified: {
    type: Boolean,
    default: false,
  },
  mobileNumberVerifiedAt: { type: Date },
  smsOtp: { type: String },
  smsOtpCreatedAt: { type: Date },
  passwordHash: { type: String, required: true },
  internalPassword: { type: String, required: true },
  resetPwCode: { type: String },
  resetPwCodeDate: { type: Date },
  role: {
    type: String,
    enum: UserRoles,
    default: "user",
  },
  deleted: {
    type: Boolean,
    default: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

export default mongoose.model<UserDeletedDoc, UserDeletedModel>("UserDeleted", schema);
