import express from "express";

import adminAuthService from "../services/adminAuth.service";
import adminCouponService from "../services/adminCoupon.service";
import adminRestaurantService from "../services/adminRestaurant.service";
import adminUserService from "../services/adminUser.service";
import userService from "../services/user.service";

const router = express.Router();

router.post("/login", adminAuthService.login);
router.get("/me", adminAuthService.me);
router.post("/verifyUser", adminAuthService.verify);
router.get("/verifyUserStatus", adminAuthService.checkVerifyStatus);

router.post("/user/add", adminUserService.addUser);
router.get("/user/get", adminUserService.get);
router.get("/user/list", userService.list);
router.post("/user/update", adminUserService.updateUser);
router.post("/user/resendInvitation", adminUserService.resendInvitation);
router.patch("/user/disable/:id", adminUserService.disable);

router.post("/restaurant/create", adminRestaurantService.create);
router.get("/restaurant/list", adminRestaurantService.list);
router.post("/restaurant/edit", adminRestaurantService.edit);
router.delete("/restaurant/delete/:id", adminRestaurantService.remove);

router.post("/coupon/create", adminCouponService.create);
router.get("/coupon/list", adminCouponService.list);
router.post("/coupon/edit", adminCouponService.edit);
router.delete("/coupon/delete/:id", adminCouponService.remove);

router.post("/coupon/redeem", adminCouponService.redeem);
router.get("/coupon/listRedeem/:id", adminCouponService.listRedeem);

router.post("/coupon/issue", adminCouponService.issue);
router.get("/coupon/listIssue/:id", adminCouponService.listIssue);
router.get("/coupon/reportRedeem", adminCouponService.reportRedeem);

export default router;
