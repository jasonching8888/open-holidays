import express from "express";

import userService from "../services/user.service";

const router = express.Router();

router.get("/hi", userService.hi);
router.delete("/user/delete", userService.deleteUser);

export default router;
