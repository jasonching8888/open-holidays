import Debug from "debug";
import express from "express";
import { body, query } from "express-validator";

import { validateResult } from "../middleware/validator.mw";
import AdminUserModel from "../models/adminUser.model";
import adminUserUtil from "../utils/adminUser.utils";

// eslint-disable-next-line
const debug = Debug("project:adminAuth.service");

const login = [
  body("email").exists().isEmail().withMessage("Invalid email address"),
  body("password").exists().withMessage("Invalid password"),
  validateResult,
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      const user = await AdminUserModel.findOne({ email: req.data.email }).lean();

      if (user === null) {
        return res.status(401).json({ message: "Unable to login.  Either your email address or password is wrong." });
      }

      const isPwValid = await adminUserUtil.verifyPassword(req.data.password, user.passwordHash);

      if (!isPwValid) {
        return res.status(401).json({ message: "Unable to login.  Either your email address or password is wrong." });
      }

      if (user.status === "pending" || user.status === "deactivated") {
        res.status(400).json({ message: "Unable to login.  Your account is not active." });
      }

      const token = await adminUserUtil.generateToken(user);

      res.json({ token });
    } catch (e) {
      next(e);
    }
  },
];

const me = [
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      // user type
      // shop list
      res.json({
        id: req.adminUser._id,
        role: req.adminUser.role,
        email: req.adminUser.email,
      });
    } catch (e) {
      next(e);
    }
  },
];

const verify = [
  body("userId").isMongoId().withMessage("invalid adminUserId"),
  body("emailVerificationCode").isString().withMessage("invalid emailVerificationCode"),
  body("password").isString().withMessage("Invalid password"),
  validateResult,
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      const status = await AdminUserModel.updateOne(
        {
          _id: req.data.userId,
          emailVerificationCode: req.data.emailVerificationCode,
        },
        {
          $set: {
            status: "created",
            emailVerificationCode: undefined,
            passwordHash: await adminUserUtil.generateHash(req.data.password),
          },
        }
      ).lean();

      if (status.modifiedCount === 1) {
        return res.json({ message: "success" });
      }

      return res.status(400).json({ message: "failed" });
    } catch (e) {
      next(e);
    }
  },
];

const checkVerifyStatus = [
  query("userId").isMongoId().withMessage("invalid userId"),
  query("emailVerificationCode").isString().withMessage("invalid emailVerificationCode"),
  validateResult,
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      const adminUser = await AdminUserModel.findOne({
        _id: req.data.userId,
        // email_verification_code: req.data.emailVerificationCode,
        // status: AdminUserStatus.pending,
      }).lean();

      if (!adminUser) {
        return res.json({ status: "failedOrNotExist" });
      }

      if (adminUser.status !== "pending") {
        return res.json({ status: "alreadyVerified" });
      }

      if (adminUser.emailVerificationCode !== req.data.emailVerificationCode) {
        return res.json({ status: "failedOrNotExist" });
      }

      res.json({ status: "pending" });
    } catch (e) {
      next(e);
    }
  },
];

export default { login, me, verify, checkVerifyStatus };
