import Debug from "debug";
import express from "express";
import { body, param } from "express-validator";

import isAdminMw from "../middleware/isAdmin.mw";
import { validateResult } from "../middleware/validator.mw";
import CouponModel, { CouponStatusArr } from "../models/coupon.model";
import { RestaurantDocLean } from "../models/restaurant.model";
import { UserDocLean } from "../models/user.model";

// eslint-disable-next-line
const debug = Debug("project:adminCoupon");

const create = [
  isAdminMw,
  body("restaurantId").isMongoId().withMessage("invalid"),
  body("code").isString().withMessage("invalid"),
  body("discount").isNumeric().withMessage("invalid"),
  body("usageLimit").isInt().withMessage("invalid"),
  validateResult,
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      await CouponModel.create({
        restaurantId: req.data.restaurantId,
        code: req.data.code,
        discount: req.data.discount,
        usageLimit: req.data.usageLimit,
      });

      res.json({ message: "success" });
    } catch (e) {
      next(e);
    }
  },
];

const list = [
  isAdminMw,
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      const list = await CouponModel.find({}).populate(["restaurantId"]).lean();
      res.json(
        list.map((l) => {
          const restaurant = l.restaurantId as RestaurantDocLean;

          return {
            id: l._id,
            code: l.code,
            discount: l.discount,
            status: l.status,
            usageLimit: l.usageLimit,
            restaurant: {
              id: restaurant?._id,
              name: restaurant?.name,
            },
            redeemCount: l.redeem.length,
            issueCount: l.issue.length,
          };
        })
      );
    } catch (e) {
      next(e);
    }
  },
];

const edit = [
  isAdminMw,
  body("id").isMongoId().withMessage("invalid"),
  body("restaurantId").isMongoId().withMessage("invalid"),
  body("code").isString().withMessage("invalid"),
  body("discount").isNumeric().withMessage("invalid"),
  body("usageLimit").isInt().withMessage("invalid"),
  body("status").isIn(CouponStatusArr).withMessage("invalid"),
  validateResult,
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      await CouponModel.findByIdAndUpdate(req.data.id, {
        $set: {
          restaurantId: req.data.restaurantId,
          code: req.data.code,
          discount: req.data.discount,
          usageLimit: req.data.usageLimit,
          status: req.data.status,
        },
      });
      res.json({ message: "success" });
    } catch (e) {
      next(e);
    }
  },
];

const remove = [
  isAdminMw,
  param("id").isMongoId().withMessage("invalid"),
  validateResult,
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      await CouponModel.deleteOne({
        _id: req.data.id,
      });
      res.json({ message: "success" });
    } catch (e) {
      next(e);
    }
  },
];

const redeem = [
  isAdminMw,
  body("couponId").isMongoId().withMessage("invalid"),
  body("userId").isMongoId().withMessage("invalid"),
  validateResult,
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      await CouponModel.updateOne(
        {
          _id: req.data.couponId,
          redeem: { $not: { $elemMatch: { userId: req.data.userId } } },
        },
        {
          $push: {
            redeem: {
              userId: req.data.userId,
            },
          },
        }
      );

      res.json({ message: "success" });
    } catch (e) {
      next(e);
    }
  },
];

const listRedeem = [
  isAdminMw,
  param("id").isMongoId().withMessage("invalid"),
  validateResult,
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      const coupon = await CouponModel.findById(req.data.id, "redeem").populate(["redeem.userId"]).lean();

      res.json(
        coupon.redeem.map((r) => {
          const user = r.userId as UserDocLean;

          return {
            id: r._id,
            at: r.at,
            user: {
              id: user._id,
              uid: user.uid,
              signInProvider: user.sign_in_provider,
            },
          };
        })
      );
    } catch (e) {
      next(e);
    }
  },
];

const issue = [
  isAdminMw,
  body("couponId").isMongoId().withMessage("invalid"),
  body("userId").isMongoId().withMessage("invalid"),
  validateResult,
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      await CouponModel.updateOne(
        {
          _id: req.data.couponId,
          issue: { $not: { $elemMatch: { userId: req.data.userId } } },
        },
        {
          $push: {
            issue: {
              userId: req.data.userId,
            },
          },
        }
      );

      res.json({ message: "success" });
    } catch (e) {
      next(e);
    }
  },
];

const listIssue = [
  isAdminMw,
  param("id").isMongoId().withMessage("invalid"),
  validateResult,
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      const coupon = await CouponModel.findById(req.data.id, "issue").populate(["issue.userId"]).lean();

      res.json(
        coupon.issue.map((r) => {
          const user = r.userId as UserDocLean;

          return {
            id: r._id,
            at: r.at,
            user: {
              id: user._id,
              uid: user.uid,
              signInProvider: user.sign_in_provider,
            },
          };
        })
      );
    } catch (e) {
      next(e);
    }
  },
];

const reportRedeem = [
  isAdminMw,
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      const coupons = await CouponModel.find({}, ["code", "redeem", "restaurantId"])
        .populate(["redeem.userId", "restaurantId"])
        .lean();

      res.json(
        coupons.map((c) => {
          return {
            id: c._id,
            code: c.code,
            restaurant: (c.restaurantId as RestaurantDocLean).name,
            redeem: c.redeem.map((r) => {
              const user = r.userId as UserDocLean;

              return {
                id: r._id,
                at: r.at,
                user: {
                  id: user._id,
                  uid: user.uid,
                  signInProvider: user.sign_in_provider,
                },
              };
            }),
          };
        })
      );
    } catch (e) {
      next(e);
    }
  },
];

export default {
  create,
  list,
  edit,
  remove,
  redeem,
  listRedeem,
  issue,
  listIssue,
  reportRedeem,
};
