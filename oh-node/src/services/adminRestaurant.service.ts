import express from "express";
import { body, param } from "express-validator";

import isAdminMw from "../middleware/isAdmin.mw";
import { validateResult } from "../middleware/validator.mw";
import RestaurantModel from "../models/restaurant.model";

const create = [
  isAdminMw,
  body("name").isString().withMessage("invalid"),
  validateResult,
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      await RestaurantModel.create({
        name: req.data.name,
      });

      res.json({ message: "success" });
    } catch (e) {
      next(e);
    }
  },
];

const list = [
  isAdminMw,
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      const list = await RestaurantModel.find({}).lean();
      res.json(
        list.map((l) => {
          return {
            id: l._id,
            name: l.name,
          };
        })
      );
    } catch (e) {
      next(e);
    }
  },
];

const edit = [
  isAdminMw,
  body("id").isMongoId().withMessage("invalid"),
  body("name").isString().withMessage("invalid"),
  validateResult,
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      await RestaurantModel.findByIdAndUpdate(req.data.id, {
        $set: {
          name: req.data.name,
        },
      });
      res.json({ message: "success" });
    } catch (e) {
      next(e);
    }
  },
];

const remove = [
  isAdminMw,
  param("id").isMongoId().withMessage("invalid"),
  validateResult,
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      await RestaurantModel.deleteOne({
        _id: req.data.id,
      });
      res.json({ message: "success" });
    } catch (e) {
      next(e);
    }
  },
];

export default {
  create,
  list,
  edit,
  remove,
};
