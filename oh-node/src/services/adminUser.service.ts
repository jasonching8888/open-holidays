import Debug from "debug";
import express from "express";
import { body, param, query } from "express-validator";

import adminUserCore from "../core/adminUser.core";
import isAdminMw from "../middleware/isAdmin.mw";
import { validateResult } from "../middleware/validator.mw";
import AdminUserModel, { AdminUserDocLean } from "../models/adminUser.model";

// eslint-disable-next-line
const debug = Debug("project:adminUser.service");

const list = [
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      const users = await AdminUserModel.find({}).lean();

      res.json(users.map(resUser));
    } catch (e) {
      next(e);
    }
  },
];

function resUser(user: AdminUserDocLean) {
  return {
    id: user._id,
    email: user.email,
    createdAt: user.createdAt,
    name: user.name,
    role: user.role,
    status: user.status,
  };
}

const addUser = [
  isAdminMw,
  body("email").isEmail().withMessage("Invalid email"),
  body("role").isIn(["admin", "regular"]).withMessage("Invalid type"),
  body("name").isString().withMessage("Invalid name"),
  validateResult,
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      const adminUser = await AdminUserModel.findOne({ email: req.data.email }).lean();

      if (adminUser) {
        return res.status(401).json({ message: "alreadyExists" });
      }

      if (adminUser === null) {
        await adminUserCore.addUserCreateNew(req.data.email, req.data.role, req.data.name);
      }

      res.json({ message: "success" });
    } catch (e) {
      next(e);
    }
  },
];

const get = [
  query("adminUserId").isMongoId().exists().withMessage("Invalid adminUser id"),
  validateResult,
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      // debug(req);
      const adminUser = await AdminUserModel.findById(req.data.adminUserId).lean();

      // debug("adminUser:", adminUser);

      res.json(adminUser);
    } catch (e) {
      next(e);
    }
  },
];

const updateUser = [
  isAdminMw,
  body("id").isMongoId().withMessage("invalid id"),
  body("email").isEmail().withMessage("Invalid email"),
  body("role").isIn(["admin", "regular"]).withMessage("Invalid type"),
  body("name").isString().withMessage("Invalid name"),
  validateResult,
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      const rst = await AdminUserModel.updateOne(
        {
          _id: req.data.id,
        },
        {
          $set: {
            email: req.data.email,
            role: req.data.role,
            name: req.data.name,
          },
        }
      );

      if (rst.matchedCount === 0) {
        return res.status(401).json({ message: "noRecord" });
      }

      if (rst.matchedCount > 1) {
        return res.status(401).json({ message: "moreThanOneRecord" });
      }

      res.json({ message: "success" });
    } catch (e) {
      next(e);
    }
  },
];

const disableUser = [
  isAdminMw,
  param("id").isMongoId().withMessage("invalid id"),
  body("isDisable").isBoolean().withMessage("invalid isDisable"),
  validateResult,

  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const user = await AdminUserModel.findByIdAndUpdate(
      { _id: req.params.id },
      { $set: { status: req.body.isDisable ? "created" : "deactivated" } },
      { new: true }
    );
    if (!user) {
      return res.status(404).json({ message: "no Record" });
    }
    res.status(200).json({ message: "success" });
  },
];

const resendInvitation = [
  body("id").isMongoId().withMessage("invalid id"),
  validateResult,
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      const adminUser = await AdminUserModel.findById(req.data.id).lean();
      await adminUserCore.emailInvitation(adminUser);

      res.json({ message: "success" });
    } catch (e) {
      next(e);
    }
  },
];

export default { list, addUser, updateUser, get, resendInvitation, disable: disableUser };
