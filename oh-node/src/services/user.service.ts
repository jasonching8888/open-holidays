import express from "express";

import isAdminMw from "../middleware/isAdmin.mw";
import UserModel, { UserDocLean } from "../models/user.model";

const env = process.env.NODE_ENV || "development";

// eslint-disable-next-line
const config = require(`../config/${env}.config`);

const hi = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
  const user = req.currentUser as UserDocLean;

  res.json({
    msg: "Hello!",
    uid: user.uid,
  });
};

const deleteUser = [
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      await UserModel.findByIdAndRemove(req.currentUser._id);

      res.json({ message: "success" });
    } catch (e) {
      next(e);
    }
  },
];

const list = [
  isAdminMw,
  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      const users = await UserModel.find().lean();

      res.json(
        users.map((u) => {
          return {
            id: u._id,
            uid: u.uid,
            signInProvider: u.sign_in_provider,
          };
        })
      );
    } catch (e) {
      next(e);
    }
  },
];

export default {
  hi,
  deleteUser,
  list,
};
