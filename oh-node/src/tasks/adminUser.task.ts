import AdminUserModel from "../models/adminUser.model";
import adminUserUtil from "../utils/adminUser.utils";

async function createSuperAdmin(email: string, plain: string, name: string) {
  return await AdminUserModel.create({
    email,
    name,
    passwordHash: await adminUserUtil.generateHash(plain),
    internalPassword: adminUserUtil.randomInternalPassword(),
    // superadmin doesn't required a password
    role: "admin",
    status: "created",
  });
}

export default {
  createSuperAdmin,
};
