import mongoose from "mongoose";
import request from "supertest";

import app, { startPromise } from "../app";
import testUtility from "./testUtility";

describe("Init Test", async () => {
  it("Drop Database", async () => {
    await startPromise;
    await mongoose.connection.db.dropDatabase();
    await mongoose.connection.syncIndexes();
  });
});

describe("Hi", async () => {
  it("Hi", async () => {
    const res = await request(app) //
      .get("/api/hi")
      .set("x-access-token", testUtility.getTokenEmployerA());

    if (res.error) {
      console.error(res.error);
    }

    res.status.should.equal(200);
    res.body.uid.should.equal("13728272");
  });
});
