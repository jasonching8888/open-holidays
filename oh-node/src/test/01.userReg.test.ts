import chai from "chai";
import Debug from "debug";
import { DecodedIdToken } from "firebase-admin/auth";
import request from "supertest";

import app from "../app";
import UserModel from "../models/user.model";
import testUtility from "./testUtility";

// eslint-disable-next-line
const debug = Debug("project:01.userReg.test");

chai.should();

describe("Delete User", () => {
  it("Create Users", async () => {
    const users = [
      { email: "zhangsan@gmail.com", uid: 12345678 },
      { email: "lisi@gmail.com", uid: 87654321 },
      { email: "wangwu@gmail.com", uid: 45678901 },
      { email: "zhaoliu@gmail.com", uid: 98765432 },
      { email: "chenqi@gmail.com", uid: 23456789 },
      { email: "zhaojin@gmail.com", uid: 54321098 },
      { email: "zhouqi@gmail.com", uid: 67890123 },
      { email: "wugang@gmail.com", uid: 89012345 },
      { email: "sungao@gmail.com", uid: 10987654 },
      { email: "linlin@gmail.com", uid: 32109877 },
      { email: "yuanliu@gmail.com", uid: 76543210 },
      { email: "hongshen@gmail.com", uid: 54321090 },
      { email: "yiyang@gmail.com", uid: 98765433 },
      { email: "yangxu@gmail.com", uid: 12345679 },
      { email: "xuexin@gmail.com", uid: 89012346 },
      { email: "songchen@gmail.com", uid: 23456780 },
      { email: "chenzhang@gmail.com", uid: 67890124 },
      { email: "wuxiao@gmail.com", uid: 10987655 },
      { email: "lilyang@gmail.com", uid: 32109876 },
      { email: "zhujiang@gmail.com", uid: 87654322 },
    ];

    for (const user of users) {
      const token: DecodedIdToken = {
        aud: "aud",
        auth_time: 123,
        exp: 123,
        email: user.email,
        firebase: {
          identities: {},
          sign_in_provider: "google.com",
        },
        uid: user.uid.toString(),
        iat: 123,
        iss: "123",
        sub: "sub",
      };

      const str = JSON.stringify(token);

      const res = await request(app) //
        .get("/api/hi")
        .set("x-access-token", str);
      res.status.should.equal(200);
    }
  });

  it("Delete User", async () => {
    const res = await request(app) //
      .delete("/api/user/delete")
      .set("x-access-token", testUtility.getTokenEmployerA());
    if (!res.error) console.error(res.error);
    res.status.should.equal(200);
  });

  it("Verify", async () => {
    const count = await UserModel.countDocuments({
      mobileNumber: "85261318613",
    });
    count.should.equal(20);
    // user.deleted.should.be.true;
  });
});
