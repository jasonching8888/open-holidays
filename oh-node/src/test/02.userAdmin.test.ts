import chai from "chai";
import Debug from "debug";
import request from "supertest";

import app from "../app";
import AdminUserModel, { AdminUserDocLean } from "../models/adminUser.model";
import MailModel from "../models/mail.model";
import adminUserTask from "../tasks/adminUser.task";
import testData from "./testData";

chai.should();

// eslint-disable-next-line
const debug = Debug("project:01.userAdmin.test");

const mainAdminUserEmail = "jason.ching@hishk.com";
const mainAdminUserPwd = "pass";

describe("User Admin Test", async () => {
  before(async () => {
    const user = await adminUserTask.createSuperAdmin(mainAdminUserEmail, mainAdminUserPwd, "Jason Ching");
    testData.adminUserJason.doc = user;
  });

  it("Login - Wrong Password", async () => {
    const res = await request(app) //
      .post("/admin/login")
      .send({
        email: mainAdminUserEmail,
        password: "wrong password",
      });

    // if (res.error) {
    //   console.error(res.error);
    // }

    res.status.should.equal(401);
    (res.body.token === undefined).should.be.true;
  });

  it("Login - Success", async () => {
    const res = await request(app) //
      .post("/admin/login")
      .send({
        email: mainAdminUserEmail,
        password: mainAdminUserPwd,
      });

    if (res.error) {
      console.error(res.error);
    }

    res.status.should.equal(200);
    (res.body.token !== undefined).should.be.true;
    testData.userJason.token = res.body.token;
  });

  it("Login - Wrong Email", async () => {
    const res = await request(app) //
      .post("/admin/login")
      .send({
        email: "lfsdlf@abc.com",
        password: "dkljew",
      });

    res.status.should.equal(401);
  });
});

describe("Add New User", () => {
  it("Add Isa as Admin User", async () => {
    const res = await request(app) //
      .post("/admin/user/add")
      .set("x-access-token", testData.userJason.token)
      .send({
        email: "isa.tsang@hishk.com",
        name: "Isa",
        role: "admin",
      });

    if (res.error) console.error(res.error);
    res.status.should.equal(200);

    // registration email should be sent out
    const user = await AdminUserModel.findOne({ email: "isa.tsang@hishk.com" }).lean();
    const mail = await MailModel.findOne({ admin_user_id: user._id });
    (mail !== null).should.be.true;

    const match = mail.message.html.match(/"(http.\S*)"/);
    const url = match[1];

    (url !== null).should.be.true;

    await userRegistrationVerify(user, "WERkak!9!@8w@@@@@@@");
  });

  it("Login - Success (Isa)", async () => {
    const res = await request(app) //
      .post("/admin/login")
      .send({
        email: "isa.tsang@hishk.com",
        password: "WERkak!9!@8w@@@@@@@",
      });

    if (res.error) console.error(res.error);

    res.status.should.equal(200);
    (res.body.token !== undefined).should.be.true;
    testData.userIsa.token = res.body.token;
  });
});

describe("User Profile Test", () => {
  it("Get User Profile", async () => {
    const res = await request(app) //
      .get("/admin/me")
      .set("x-access-token", testData.userJason.token);

    if (res.error) console.error(res.error);
    res.status.should.equal(200);

    debug("role", res.body);
    res.body.role.should.equal("admin");
    res.body.email.should.equal(mainAdminUserEmail);
  });
});

describe("Admin User Read/Modify", () => {
  it("Read Admin Users List", async () => {
    const res = await request(app) //
      .get(`/admin/user/list`)
      .set("x-access-token", testData.userJason.token);

    if (res.error) console.error(res.error);
    res.status.should.equal(200);
    testData.usersAll.users = res.body;
    console.log(testData.usersAll.users);
  });

  it("Read Admin User", async () => {
    const id = testData.usersAll.users[0].id as string;
    const res = await request(app) //
      .get(`/admin/user/get?adminUserId=${id}`)
      .set("x-access-token", testData.userJason.token);

    if (res.error) console.error(res.error);
    res.status.should.equal(200);
  });
});

export async function userRegistrationVerify(user: AdminUserDocLean, password: string) {
  const mail = await MailModel.findOne({ admin_user_id: user._id });
  (mail !== null).should.be.true;

  const match = mail.message.html.match(/"(http.\S*)"/);
  const url = match[1];

  (url !== null).should.be.true;

  const userId = user._id.toString() as string;
  let res = await request(app) //
    .get(`/admin/verifyUserStatus?userId=${userId}&emailVerificationCode=${user.emailVerificationCode}`);

  if (res.error) console.error(res.error);

  res.status.should.equal(200);
  res.body.status.should.equal("pending");

  res = await request(app) //
    .post("/admin/verifyUser")
    .send({
      userId: user._id,
      emailVerificationCode: user.emailVerificationCode,
      password,
    });

  if (res.error) console.error(res.error);
  res.status.should.equal(200);
}
