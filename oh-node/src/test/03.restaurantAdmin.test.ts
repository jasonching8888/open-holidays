import chai from "chai";
import Debug from "debug";
import request from "supertest";

import app from "../app";
import testData from "./testData";

chai.should();

// eslint-disable-next-line
const debug = Debug("project:03.restaurantAdmin.test");

describe("Restarutant Admin Test", () => {
  let restaurantId: string;

  it("Create", async () => {
    const names = [
      "McDonald's",
      "MOS Burger",
      "蟹仙海鮮私房",
      "Gram cafe & pancakes",
      "The Captain's House",
      "Outdark (厚福街)",
      "Lady Nara",
      "Mamaday",
      "十和田總本店",
      "鐵板．壹",
      "JUST'er Bar & Restaurant",
      "Nara Thai Cuisine (港威商場)",
      "RUE DU LIBAN",
      "The Grill Room (The Humphreys)",
      "OPPA Coffee Bar Restaurant",
      "八海山",
      "MUTA Restaurant by ATUM",
      "韓屋",
      "hoo韓國包車",
    ];

    for (const name of names) {
      const res = await request(app) //
        .post("/admin/restaurant/create")
        .set("x-access-token", testData.userJason.token)
        .send({
          name,
        });

      res.status.should.equal(200);
    }
  });

  // it("Create - One More", async () => {
  //   const res = await request(app) //
  //     .post("/admin/restaurant/create")
  //     .set("x-access-token", testData.userJason.token)
  //     .send({
  //       name: "MOS Burger",
  //     });

  //   res.status.should.equal(200);
  // });

  it("List", async () => {
    const res = await request(app) //
      .get("/admin/restaurant/list")
      .set("x-access-token", testData.userJason.token);

    res.status.should.equal(200);
    res.body.length.should.equal(19);

    restaurantId = res.body[0].id;
    testData.restaurantId = res.body[1].id;
  });

  it("Edit", async () => {
    const res = await request(app) //
      .post("/admin/restaurant/edit")
      .set("x-access-token", testData.userJason.token)
      .send({
        id: restaurantId,
        name: "In-N-Out",
      });

    res.status.should.equal(200);
  });

  it("List", async () => {
    const res = await request(app) //
      .get("/admin/restaurant/list")
      .set("x-access-token", testData.userJason.token);

    res.status.should.equal(200);
    res.body.length.should.equal(19);

    res.body[0].name.should.equal("In-N-Out");
  });

  it("Delete", async () => {
    const res = await request(app) //
      .delete(`/admin/restaurant/delete/${restaurantId}`)
      .set("x-access-token", testData.userJason.token);
    // .send({
    //   id: restaurantId,
    // });

    res.status.should.equal(200);
  });

  it("List", async () => {
    const res = await request(app) //
      .get("/admin/restaurant/list")
      .set("x-access-token", testData.userJason.token);

    res.status.should.equal(200);
    res.body.length.should.equal(18);
  });
});
