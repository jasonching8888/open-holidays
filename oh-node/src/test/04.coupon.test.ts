import chai from "chai";
import Debug from "debug";
import request from "supertest";

import app from "../app";
import UserModel from "../models/user.model";
import testData from "./testData";
import testUtility from "./testUtility";

chai.should();

// eslint-disable-next-line
const debug = Debug("project:04.couponAdmin.test");

describe("Coupon Admin Test", () => {
  let couponId: string;

  it("Create", async () => {
    const res = await request(app) //
      .post("/admin/coupon/create")
      .set("x-access-token", testData.userJason.token)
      .send({
        restaurantId: testData.restaurantId,
        code: "100OFF",
        discount: 100,
        usageLimit: 10,
      });

    res.status.should.equal(200);
  });

  it("Create - One More", async () => {
    const res = await request(app) //
      .post("/admin/coupon/create")
      .set("x-access-token", testData.userJason.token)
      .send({
        restaurantId: testData.restaurantId,
        code: "200OFF",
        discount: 200,
        usageLimit: 10,
      });

    res.status.should.equal(200);
  });

  it("List", async () => {
    const res = await request(app) //
      .get("/admin/coupon/list")
      .set("x-access-token", testData.userJason.token);

    res.status.should.equal(200);
    res.body.length.should.equal(2);

    couponId = res.body[0].id;
  });

  it("Edit", async () => {
    const res = await request(app) //
      .post("/admin/coupon/edit")
      .set("x-access-token", testData.userJason.token)
      .send({
        id: couponId,
        restaurantId: testData.restaurantId,
        code: "50OFF",
        discount: 50,
        usageLimit: 11,
        status: "disabled",
      });

    res.status.should.equal(200);
  });

  it("List", async () => {
    const res = await request(app) //
      .get("/admin/coupon/list")
      .set("x-access-token", testData.userJason.token);

    res.status.should.equal(200);
    res.body.length.should.equal(2);

    res.body[0].code.should.equal("50OFF");
  });

  it("Delete", async () => {
    const res = await request(app) //
      .delete(`/admin/restaurant/delete/${couponId}`)
      .set("x-access-token", testData.userJason.token);

    res.status.should.equal(200);
  });

  it("List", async () => {
    const res = await request(app) //
      .get("/admin/restaurant/list")
      .set("x-access-token", testData.userJason.token);

    res.status.should.equal(200);
    res.body.length.should.equal(18);
  });

  it("Redeem User", async () => {
    const user = await UserModel.findOne().lean();

    const res = await request(app) //
      .post("/admin/coupon/redeem")
      .set("x-access-token", testData.userJason.token)
      .send({
        couponId,
        userId: user._id,
      });

    if (res.error) console.error(res.error);
    res.status.should.equal(200);
  });

  it("Redeem List", async () => {
    const res = await request(app) //
      .get(`/admin/coupon/listRedeem/${couponId}`)
      .set("x-access-token", testData.userJason.token);

    if (res.error) console.log(res.error);
    res.status.should.equal(200);
  });
});
