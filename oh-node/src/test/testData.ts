import { AdminUserDocLean } from "../models/adminUser.model";
import { UserDocLean } from "../models/user.model";

const data = {
  adminUserJason: {
    doc: undefined as AdminUserDocLean,
    token: "",
  },
  // List of all users
  usersAll: {
    users: undefined as AdminUserDocLean[],
  },
  userJason: {
    doc: undefined as UserDocLean,
    token: "",
    resetToken: "",
    vehicles: [] as Array<{ id: string; plate: string }>,
  },
  userIsa: {
    doc: undefined as UserDocLean,
    token: "",
  },
  userAdmin: {
    doc: undefined as UserDocLean,
    token: "",
  },
  restaurantId: "",
};

export default data;
