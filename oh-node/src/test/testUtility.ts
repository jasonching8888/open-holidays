import chai from "chai";
import { DecodedIdToken } from "firebase-admin/auth";
import mongoose from "mongoose";
import request from "supertest";

import app from "../app";
import config from "../config/test.config";
import UserModel, { UserType } from "../models/user.model";

chai.should();

function getTokenEmployerA() {
  const token: DecodedIdToken = {
    aud: "aud",
    auth_time: 123,
    exp: 123,
    email: "jason.ching@hishk.com",
    firebase: {
      identities: {},
      sign_in_provider: "google.com",
    },
    uid: "13728272",
    iat: 123,
    iss: "123",
    sub: "sub",
  };

  return JSON.stringify(token);
}

function getTokenHelperA() {
  const token: DecodedIdToken = {
    aud: "aud",
    auth_time: 123,
    exp: 123,
    email: "irene@abc.com",
    firebase: {
      identities: {},
      sign_in_provider: "google.com",
    },
    uid: "2323423",
    iat: 123,
    iss: "123",
    sub: "sub",
  };

  return JSON.stringify(token);
}

function getTokenHelperB() {
  const token: DecodedIdToken = {
    aud: "aud",
    auth_time: 123,
    exp: 123,
    email: "irene2@abc.com",
    firebase: {
      identities: {},
      sign_in_provider: "google.com",
    },
    uid: "2323423234234",
    iat: 123,
    iss: "123",
    sub: "sub",
  };

  return JSON.stringify(token);
}

function getToken(uid: string) {
  const token: DecodedIdToken = {
    aud: "aud",
    auth_time: 123,
    exp: 123,
    email: "admin@abc.com",
    firebase: {
      identities: {},
      sign_in_provider: "google.com",
    },
    uid,
    iat: 123,
    iss: "123",
    sub: "sub",
  };

  return JSON.stringify(token);
}

function getTokenAdminA() {
  const token: DecodedIdToken = {
    aud: "aud",
    auth_time: 123,
    exp: 123,
    email: "admin@abc.com",
    firebase: {
      identities: {},
      sign_in_provider: "google.com",
    },
    uid: "2817191",
    iat: 123,
    iss: "123",
    sub: "sub",
  };

  return JSON.stringify(token);
}

async function getUserInfo(token: string) {
  const res = await request(app) //
    .get("/api/user/info")
    .set("x-access-token", token);

  if (res.error) {
    console.log(res.error);
  }

  res.status.should.equal(200);

  return res.body;
}

async function setVisibility(token: string, visibility: string) {
  const res = await request(app) //
    .post("/api/user/setVisibility")
    .set("x-access-token", token)
    .send({
      visibility,
    });

  if (res.error) {
    console.log(res.error);
  }

  res.status.should.equal(200);

  return res.body;
}

async function dropDatabase() {
  mongoose.Promise = global.Promise;
  await mongoose.connect(config.database);
  await mongoose.connection.db.dropDatabase();
}

async function createHelper(profile: any) {
  await UserModel.create({
    sign_in_provider: "Google",
    uid: "1",
    helper_profile: profile,
    user_type: UserType.helper,
  });
}

async function createEmployer(profile: any) {
  await UserModel.create({
    sign_in_provider: "Google",
    uid: "1",
    employer_profile: profile,
    user_type: UserType.employer,
  });
}

async function getAllMatches(token: string) {
  const res = await request(app) //
    .get("/api/match/admin/matchList")
    .set("x-access-token", token);

  res.status.should.equal(200);
  return res.body;
}

async function scheduleSend(token: string, data: { matchId: string; dateTime: Date; meetingUrl: string }) {
  const res = await request(app) //
    .post("/api/schedule/send")
    .set("x-access-token", token)
    .send({
      matchId: data.matchId,
      dateTime: data.dateTime,
      meetingUrl: data.meetingUrl,
    });

  res.status.should.equal(200);
  return res.body;
}

async function addDeviceToken(accessToken: string, token: string, locale: string) {
  const res = await request(app) //
    .post("/api/user/addDeviceToken")
    .set("x-access-token", accessToken)
    .send({
      token: token,
      locale: locale,
    });

  res.status.should.equal(200);
}

async function sendOtp(accessToken: string, tel: string, expectError = false) {
  const res = await request(app) //
    .post("/api/user/sendOtp")
    .set("x-access-token", accessToken)
    .send({
      tel,
    });

  if (!expectError) {
    if (res.error) console.error(res.error);
    res.status.should.equal(200);
  }

  return res;
}

async function verifyOtp(accessToken: string, otp: string, expectedError = false) {
  const res = await request(app) //
    .post("/api/user/verifyOtp")
    .set("x-access-token", accessToken)
    .send({
      otp,
    });

  if (!expectedError) {
    res.status.should.equal(200);
  }

  return res;
}

export default {
  getTokenEmployerA,
  getTokenHelperA,
  getTokenHelperB,
  getTokenAdminA,
  getToken,
  dropDatabase,
  getUserInfo,
  setVisibility,
  createHelper,
  createEmployer,
  getAllMatches,
  scheduleSend,
  addDeviceToken,
  sendOtp,
  verifyOtp,
};
