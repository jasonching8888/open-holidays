declare namespace Express {
  export enum AdminUserType {
    superAdmin = "superAdmin",
    regular = "regular",
  }

  export interface Request {
    data: any;
    adminUser: {
      _id: any;
      role: "admin" | "regular";
      email: string;
    };
    currentUser: any;
    locale: string;
  }
}
