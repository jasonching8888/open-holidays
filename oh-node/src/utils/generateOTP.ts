export default function generateSmsOtp(digits = 6) {
  let rst = "";

  for (let i = 0; i < digits; i++) {
    rst = rst + Math.floor(Math.random() * 10).toString();
  }

  return rst;
}
