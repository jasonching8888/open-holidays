const messages = {
  resetPwCode: `Hello,

To change your password, please use the following verification code: {code}. This code is valid for the next 15 minutes.

If you didn't request this password change, please contact support immediately.

Thank you!
`,

  sms_otp: `Hello,

Welcome to Dr Motor!

To complete your registration, please use the following verification code: {otp}. This code is valid for the next 15 minutes.

If you didn't initiate this registration, please contact support immediately.

Thank you.
`
};

export default messages;
